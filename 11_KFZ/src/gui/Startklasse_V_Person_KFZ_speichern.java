package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Auto;
import fachklasse.Datenbankverbindung;
import fachklasse.Person;

public class Startklasse_V_Person_KFZ_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlPersonennummer;
	private JLabel jlKFZNummer;
	private JLabel jlAusgabe;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;
	private JComboBox<String> meineJCBKFZ;
	private JComboBox<String> meineJCBPerson;
	private ArrayList<Person> meinePersonenListe;
	private ArrayList<Auto> meineKFZListe;
	
	public static void main(String[] args)
	{
		new Startklasse_V_Person_KFZ_speichern().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Person und Auto verkn�pfen");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlPersonennummer = new JLabel();
		jlPersonennummer.setBounds(20, 20, 200, 20);
		jlPersonennummer.setText("Personen: ");
		frame.add(jlPersonennummer);
		
		jlKFZNummer = new JLabel();
		jlKFZNummer.setBounds(20, 60, 200, 20);
		jlKFZNummer.setText("KFZ: ");
		frame.add(jlKFZNummer);
		
		meineJCBKFZ = new JComboBox<>();
		meineJCBKFZ.setBounds(120, 60, 200, 20);
		meineJCBKFZ.addItem("Bitte ein KFZ ausw�hlen");
		frame.add(meineJCBKFZ);
		
		meineJCBPerson = new JComboBox<>();
		meineJCBPerson.setBounds(120, 20, 200, 20);
		meineJCBPerson.addItem("Bitte eine Person ausw�hlen");
		frame.add(meineJCBPerson);
		
		//************************************************
		// ComboBox f�llen
		meineDBV = new Datenbankverbindung();
		meinePersonenListe = meineDBV.sucheAllePersonen();
		for (int i = 0; i < meinePersonenListe.size(); i++)
		{
			Person meinePerson = meinePersonenListe.get(i);
			meineJCBPerson.addItem(meinePerson.getiNr()+" "+meinePerson.getsVorname()+" "+meinePerson.getsNachname());
		}

		
		meineKFZListe = meineDBV.sucheAlleKFZ();
		for (int i = 0; i < meineKFZListe.size(); i++)
		{
			Auto meinAuto = meineKFZListe.get(i);
			meineJCBKFZ.addItem(meinAuto.getNummer()+" "+meinAuto.getMarke()+" "+meinAuto.getModell());
		}
		
		
		//************************************************
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 140, 270, 26);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// ComboBox Person auslesen
		int iZeilePerson = meineJCBPerson.getSelectedIndex();
		Person meinePerson = meinePersonenListe.get(iZeilePerson-1);
		
		// ComboBox KFZ auslesen
		int iZeileKFZ = meineJCBKFZ.getSelectedIndex();
		Auto meinAuto = meineKFZListe.get(iZeileKFZ-1);
		
		// Die beiden Zahlen (KFZ-Nr und Personen-Nr) in die Datenbank speichern
		boolean bHatGeklappt = meineDBV.speichere_V_Person_KFZ(meinePerson.getiNr(), meinAuto.getNummer());
		
		if (bHatGeklappt == true)
		{
			jlAusgabe.setText("Speichern erfolgreich");
			Startklasse_V_Person_KFZ_speichern.main(null);  
			frame.dispose();
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Speichern fehlgeschlagen");
		}
		
		
		
	}

	
}
