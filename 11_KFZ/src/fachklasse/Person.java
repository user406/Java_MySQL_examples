package fachklasse;

public class Person
{
	private int iNr;
	private String sVorname;
	private String sNachname;
	
	public int getiNr()
	{
		return iNr;
	}
	public void setiNr(int iNr)
	{
		this.iNr = iNr;
	}
	public String getsVorname()
	{
		return sVorname;
	}
	public void setsVorname(String sVorname)
	{
		this.sVorname = sVorname;
	}
	public String getsNachname()
	{
		return sNachname;
	}
	public void setsNachname(String sNachname)
	{
		this.sNachname = sNachname;
	}
	
}
