package fachklasse;

public class Auto
{
	private int iNummer;
	private String sMarke;
	private String sModell;
	private double dPreis;
	
	public void setNummer (int iNr)
	{
		iNummer = iNr;
	}
	
	public void setMarke (String sMark)
	{
		sMarke = sMark;
	}
	
	public void setModell (String sMod)
	{
		sModell = sMod;
	}
	
	public void setPreis (double dPr)
	{
		dPreis = dPr;
	}
	
	public int getNummer()
	{
		return iNummer;
	}
	
	public String getMarke()
	{
		return sMarke;
	}
	
	public String getModell()
	{
		return sModell;
	}
	
	public double getPreis()
	{
		return dPreis;
	}
}
