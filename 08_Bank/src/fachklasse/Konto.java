package fachklasse;

public class Konto
{
	int iBLZ;
	int iKontonummer;
	String sBIC;
	String sIBAN;
	
	public int getiBLZ()
	{
		return iBLZ;
	}
	
	public void setiBLZ(int iBLZ)
	{
		this.iBLZ = iBLZ;
	}
	
	public int getiKontonummer()
	{
		return iKontonummer;
	}
	
	public void setiKontonummer(int iKontonummer)
	{
		this.iKontonummer = iKontonummer;
	}
	
	public String getsBIC()
	{
		return sBIC;
	}
	
	public void setsBIC(String sBIC)
	{
		this.sBIC = sBIC;
	}
	
	public String getsIBAN()
	{
		return sIBAN;
	}
	
	public void setsIBAN(String sIBAN)
	{
		this.sIBAN = sIBAN;
	}
	
}
