package fachklasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;

	// Methode, die sich mit der Datenbank verbindet
	private void verbindeMitMySQL()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			System.out.println("Drivermanager geladen, Treibername ok");

			try
			{
				meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_bank?user=root&password=");
				meinStatement = meineConnection.createStatement();
				System.out.println("Connection etabliert");

			}
			catch (SQLException eSQL)
			{
				System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
				System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
				System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
			}

		}
		catch (Exception e)
		{
			System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
			System.out.println("Treibername falsch - VendorError: \n" + e.toString());
		}
	}

	// Methode, die die MySQL-Verbindung beendet
	private void beendeMySQLVerbindung()
	{
		try
		{
			meinStatement.close();
			meineConnection.close();
			System.out.println("Verbindung beendet");
		}
		catch (SQLException e)
		{
			System.out.println("Fehler beim Beenden der Verbindung :\n" + e);
		}
	}

	// Methode, die Daten aus der Datenbank ausliest
	private ResultSet leseDaten(String sSQLAnweisung)
	{
		ResultSet meinRS = null;
		try
		{
			meinRS = meinStatement.executeQuery(sSQLAnweisung);
			System.out.println("Leseanfrage ausgef�hrt: " + sSQLAnweisung);
		}
		catch (SQLException eSQL)
		{
			System.out.println("Fehler bei der Leseanfrage: \n" + eSQL);
			meinRS = null;
		}
		return meinRS;
	}

	// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der
	// ver�nderten Zeilen in der
	// Datenbank zur�ck
	private int schreibeDaten(String sSQLAnweisung)
	{
		int iZeilen = 0;
		try
		{
			iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
			System.out.println("Schreibbefehl ausgef�hrt: " + sSQLAnweisung);
		}
		catch (SQLException eSQL)
		{
			System.out.println("Fehler beim Schreibbefehl: \n" + eSQL);
			System.out.println("\n " + sSQLAnweisung);
		}
		return iZeilen;
	}
	//********************************************************************************************
	
	// Methode, die Kunden in Datenbank speichert
	public boolean speichereKunden (String sVorname, String sNachname, String sGeburtsdatum, String sGeburtsort)
	{
		boolean bHatgeklappt = false;
		
		String sSQLAnweisung = "INSERT INTO kunde (vorname, nachname, geburtsdatum, geburtsort) VALUES ('"+sVorname+"', '"+sNachname+"', '"+sGeburtsdatum+"', '"+sGeburtsort+"');";
		
		try
		{
			this.verbindeMitMySQL();
			int iZeilen = this.schreibeDaten(sSQLAnweisung);
			
			if (iZeilen == 1)
			{
				bHatgeklappt = true;
			}
			
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Speichern des Kunden.\n"+e.toString());
		}
		finally 
		{
			this.beendeMySQLVerbindung();
		}
		
		return bHatgeklappt;
	}
	
	//***************************************************************************************************
	
	// Methode, die Konto in Datenbank speichert
	public boolean speichereKonto (int iBLZ, int iKontonummer, String sBIC, String sIBAN)
	{
		boolean bHatgeklappt = false;
		
		String sSQLAnweisung = "INSERT INTO konto (blz, kontonummer, bic, iban) VALUES ('"+iBLZ+"', '"+iKontonummer+"', '"+sBIC+"', '"+sIBAN+"');";
		
		try
		{
			this.verbindeMitMySQL();
			int iZeilen = this.schreibeDaten(sSQLAnweisung);
			
			if (iZeilen == 1)
			{
				bHatgeklappt = true;
			}
			
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Speichern des Kontos.\n"+e.toString());
		}
		finally 
		{
			this.beendeMySQLVerbindung();
		}
		
		return bHatgeklappt;
	}
	
	//******************************************************************************************
	
	// Methode, die alle Eintr�ge in der Tabelle Kunde sucht
	public ArrayList<Kunde> sucheAlleKunden ()
	{
		ArrayList<Kunde> meineKundenListe = new ArrayList<>();
		Kunde meinKunde = null;
		
		String sSQLAnweisung = "SELECT nr, vorname, nachname, geburtsdatum, geburtsort FROM kunde;";
		
		try
		{
			this.verbindeMitMySQL();
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			while (meinRS.next() == true)
			{
				meinKunde = new Kunde();
				meinKunde.setInr(meinRS.getInt("nr"));
				meinKunde.setsVorname(meinRS.getString("vorname"));
				meinKunde.setsNachname(meinRS.getString("nachname"));
				meinKunde.setsGeburtsdatum(meinRS.getString("geburtsdatum"));
				meinKunde.setsGeburtsort(meinRS.getString("geburtsort"));
				meineKundenListe.add(meinKunde);
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen aller Kunden.\n"+e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return meineKundenListe;
	}
	
	// Methode, die alle Eintr�ge in der Tabelle Konto sucht
	public ArrayList<Konto> sucheAlleKonten ()
	{
		ArrayList<Konto> meineKontoListe = new ArrayList<>();
		Konto meinKonto = null;
		
		String sSQLAnweisung = "SELECT blz, kontonummer, bic, iban FROM konto;";
		
		try
		{
			this.verbindeMitMySQL();
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			while (meinRS.next() == true)
			{
				meinKonto = new Konto();
				meinKonto.setiBLZ(meinRS.getInt("blz"));
				meinKonto.setiKontonummer(meinRS.getInt("kontonummer"));
				meinKonto.setsBIC(meinRS.getString("bic"));
				meinKonto.setsIBAN(meinRS.getString("iban"));
				meineKontoListe.add(meinKonto);
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen aller Kunden.\n"+e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return meineKontoListe;
	}
	
	public boolean speichere_V_Kunde_Konto (int iKundenNr, int iKontoNr)
	{
		boolean bHatGeklappt = false;
		
		String sSQLAnweisung  = "INSERT INTO v_kunde_konto (kunde_nr, konto_kontonummer) VALUES ("+iKundenNr+", "+iKontoNr+");";
		
		try
		{
			this.verbindeMitMySQL();
			int iZeilen = this.schreibeDaten(sSQLAnweisung);
			if (iZeilen == 1)
			{
				bHatGeklappt = true;
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Speichern in die Verkn�pfungstabelle.\n"+e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return bHatGeklappt;
	}
	
	//*********************************************************************************************************
	
	public ArrayList<Kunde> sucheKundePerName (String sName)
	{
		ArrayList<Kunde> meineKundenListe = new ArrayList<>();
		Kunde meinKunde = null;
		
		String sSQLAnweisung = "SELECT nr, vorname, nachname, geburtsdatum, geburtsort FROM kunde WHERE vorname LIKE '%"+sName+"%' OR nachname LIKE '%"+sName+"%';";
		
		try
		{
			this.verbindeMitMySQL();
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			while (meinRS.next() == true)
			{
				meinKunde = new Kunde();
				meinKunde.setInr(meinRS.getInt("nr"));
				meinKunde.setsVorname(meinRS.getString("vorname"));
				meinKunde.setsNachname(meinRS.getString("nachname"));
				meinKunde.setsGeburtsdatum(meinRS.getString("geburtsdatum"));
				meinKunde.setsGeburtsort(meinRS.getString("geburtsort"));
				meineKundenListe.add(meinKunde);
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen des Kunden per Name.\n"+e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return meineKundenListe;
	}
	
	// ****************************************************************************************************
	
	public int sucheKontoNummer (int iNr)
	{
		int iKontonummer = 0;
		Konto meinKonto = null;
		
		String sSQLAnweisung = "SELECT konto_kontonummer FROM v_kunde_konto WHERE kunde_nr = "+iNr+";";
		
		try
		{
			this.verbindeMitMySQL();
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			while (meinRS.next() == true)
			{
				iKontonummer = meinRS.getInt("konto_kontonummer");
			}			
			
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen der Kontonummer.\n"+e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		
		return iKontonummer;
	}
	
	//**************************************************************************************
	
	public ArrayList<Konto> sucheKontoDatenPerKontonummer (int iKontonummer)
	{
		ArrayList<Konto> meineKontoListe = new ArrayList<>();
		Konto meinKonto = null;
		
		String sSQLAnweisung = "SELECT blz, kontonummer, bic, iban FROM konto WHERE kontonummer LIKE '%"+iKontonummer+"%' ;";
		
		try
		{
			this.verbindeMitMySQL();
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			while (meinRS.next() == true)
			{
				meinKonto = new Konto();
				meinKonto.setiBLZ(meinRS.getInt("blz"));
				meinKonto.setiKontonummer(meinRS.getInt("kontonummer"));
				meinKonto.setsBIC(meinRS.getString("bic"));
				meinKonto.setsIBAN(meinRS.getString("iban"));
				meineKontoListe.add(meinKonto);
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen aller Kunden.\n"+e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return meineKontoListe;
	}
	
	
	
	
}
