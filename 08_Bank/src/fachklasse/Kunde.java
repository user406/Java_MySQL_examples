package fachklasse;

public class Kunde
{
	int inr;
	String sVorname;
	String sNachname;
	String sGeburtsdatum;
	String sGeburtsort;

	public int getInr()
	{
		return inr;
	}
	
	public void setInr(int inr)
	{
		this.inr = inr;
	}
	
	public String getsVorname()
	{
		return sVorname;
	}
	
	public void setsVorname(String sVorname)
	{
		this.sVorname = sVorname;
	}
	
	public String getsNachname()
	{
		return sNachname;
	}
	
	public void setsNachname(String sNachname)
	{
		this.sNachname = sNachname;
	}
	
	public String getsGeburtsdatum()
	{
		return sGeburtsdatum;
	}
	
	public void setsGeburtsdatum(String sGeburtsdatum)
	{
		this.sGeburtsdatum = sGeburtsdatum;
	}
	
	public String getsGeburtsort()
	{
		return sGeburtsort;
	}
	
	public void setsGeburtsort(String sGeburtsort)
	{
		this.sGeburtsort = sGeburtsort;
	}
		
}
