package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Kunde;

public class Startklasse_Suchen_Kunde implements ActionListener
{
	private JLabel jlName;
	private JTextField jtfName;
	private JButton jbSuchen;
	private JTextArea jtaAusgabe;
	private JScrollPane jspAusgabe;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Suchen_Kunde().go();
	}

	private void go()
	{
		JFrame frame = new JFrame();
		frame.setBounds(400, 300, 420, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Kunden suchen");
		frame.setLayout(null);
		
		jlName = new JLabel();
		jlName.setText("Vorname / Nachname:");
		jlName.setBounds(20, 20, 150, 20);
		frame.add(jlName);
		
		jtfName = new JTextField();
		jtfName.setBounds(160, 20, 180, 20);
		frame.add(jtfName);
		
		jbSuchen = new JButton();
		jbSuchen.setText("Suchen");
		jbSuchen.addActionListener(this);
		jbSuchen.setBounds(20, 60, 320, 25);
		frame.add(jbSuchen);
		
		jtaAusgabe = new JTextArea();
		jspAusgabe = new JScrollPane();
		jspAusgabe.setBounds(20, 100, 320, 240);
		jspAusgabe.setViewportView(jtaAusgabe);
		frame.add(jspAusgabe);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
		
		ArrayList<Kunde> meineKundenListe = meineDBV.sucheKundePerName(jtfName.getText());
		
		if (meineKundenListe.size() == 0)
		{
			jtaAusgabe.setForeground(Color.RED);
			jtaAusgabe.setText("Keinen Kunden gefunden.");
		}
		else
		{
			jtaAusgabe.setFont(jtaAusgabe.getFont().deriveFont(Font.BOLD));
			jtaAusgabe.setText("Folgende Kunden gefunden: \n");
			for (int i = 0; i < meineKundenListe.size(); i++)
			{
				Kunde meinKunde = meineKundenListe.get(i);
				jtaAusgabe.append("Nr: " + meinKunde.getInr() + " | " + "Vorname: " + meinKunde.getsVorname() + " | " + "Nachname: " + meinKunde.getsNachname() + " | " + "Geburtsdatum: " + meinKunde.getsGeburtsdatum() + " | " + "Geburtsort: " + meinKunde.getsGeburtsort());				
			}
		}
		
	}

}
