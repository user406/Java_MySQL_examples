package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Konto;

public class Startklasse_Suchen_Konto implements ActionListener
{
	private JFrame frame;
	private JLabel jlKontonummer;
	private JTextField jtfKontonummer;
	private JButton jbSuchen;
	private JTextArea jtaAusgabe;
	private JScrollPane jspAusgabe;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Suchen_Konto().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Kontodaten suchen");
		frame.setBounds(400, 300, 350, 400);
		
		jlKontonummer = new JLabel();
		jlKontonummer.setText("Kontonummer: ");
		jlKontonummer.setBounds(20, 20, 100, 20);
		frame.add(jlKontonummer);
		
		jtfKontonummer = new JTextField();
		jtfKontonummer.setBounds(120, 20, 180, 20);
		frame.add(jtfKontonummer);
		
		jbSuchen = new JButton();
		jbSuchen.setText("Suchen");
		jbSuchen.addActionListener(this);
		jbSuchen.setBounds(20, 60, 280, 25);
		frame.add(jbSuchen);
		
		jtaAusgabe = new JTextArea();
		jspAusgabe = new JScrollPane();
		jspAusgabe.setBounds(20, 100, 280, 240);
		jspAusgabe.setViewportView(jtaAusgabe);
		frame.add(jspAusgabe);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		int iKontonummer = Integer.parseInt(jtfKontonummer.getText());
		
		meineDBV = new Datenbankverbindung();
		
		ArrayList<Konto> meineKontoListe = new ArrayList<>();
		meineKontoListe = meineDBV.sucheKontoDatenPerKontonummer(iKontonummer);
		
		if (meineKontoListe.size() == 0)
		{
			jtaAusgabe.setForeground(Color.RED);
			jtaAusgabe.setFont(jtaAusgabe.getFont().deriveFont(Font.BOLD));
			jtaAusgabe.setText("Keine Kontodaten gefunden.");
		}
		else
		{
			jtaAusgabe.setForeground(Color.BLACK);
			jtaAusgabe.setFont(jtaAusgabe.getFont().deriveFont(Font.BOLD));
			jtaAusgabe.setText("Folgende Kontodaten gefunden: \n");
			for (int i = 0; i < meineKontoListe.size(); i++)
			{
				Konto meinKonto = meineKontoListe.get(i);
				jtaAusgabe.append("Kontonummer: " + meinKonto.getiKontonummer() + " | " + "BLZ: " + meinKonto.getiBLZ() + " | " + "BIC: " + meinKonto.getsBIC() + " | " + "IBAN: " + meinKonto.getsIBAN() + "\n");
			}
		}
		
	}

}
