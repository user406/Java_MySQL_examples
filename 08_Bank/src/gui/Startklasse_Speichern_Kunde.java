package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Speichern_Kunde implements ActionListener
{
	private JFrame frame;
	private JLabel jlVorname, jlNachname, jlGeburtsdatum, jlGeburtsort, jlAusgabe;
	private JTextField jtfVorname, jtfNachname, jtfGeburtsdatum, jtfGeburtsort;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Speichern_Kunde().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Kunde speichern");
		frame.setLayout(null);
		frame.setBounds(600, 300, 450, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 20, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jlNachname = new JLabel();
		jlNachname.setBounds(20, 60, 100, 20);
		jlNachname.setText("Nachname: ");
		frame.add(jlNachname);
		
		jlGeburtsdatum = new JLabel();
		jlGeburtsdatum.setBounds(20, 100, 100, 20);
		jlGeburtsdatum.setText("Geburtsdatum: ");
		frame.add(jlGeburtsdatum);
		
		jlGeburtsort = new JLabel();
		jlGeburtsort.setBounds(20, 140, 100, 20);
		jlGeburtsort.setText("Geburtsort: ");
		frame.add(jlGeburtsort);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(120, 20, 150, 20);
		frame.add(jtfVorname);
		
		jtfNachname = new JTextField();
		jtfNachname.setBounds(120, 60, 150, 20);
		frame.add(jtfNachname);
		
		jtfGeburtsdatum = new JTextField();
		jtfGeburtsdatum.setBounds(120, 100, 150, 20);
		frame.add(jtfGeburtsdatum);
		
		jtfGeburtsort = new JTextField();
		jtfGeburtsort.setBounds(120, 140, 150, 20);
		frame.add(jtfGeburtsort);
		
		jbSpeichern = new JButton();
		jbSpeichern.addActionListener(this);
		jbSpeichern.setText("Speichern");
		jbSpeichern.setBounds(20, 180, 250, 25);
		frame.add(jbSpeichern);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 220, 200, 20);
		jlAusgabe.setText("");
		frame.add(jlAusgabe);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sVorname = jtfVorname.getText();
		String sNachname = jtfNachname.getText();
		String sGeburtsdatum = jtfGeburtsdatum.getText();
		String sGeburtsort = jtfGeburtsort.getText();
		
		meineDBV = new Datenbankverbindung();
		boolean bHatgeklappt = meineDBV.speichereKunden(sVorname, sNachname, sGeburtsdatum, sGeburtsort);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern");
		}
		
	}

}
