package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Speichern_Konto implements ActionListener
{
	private JFrame frame;
	private JLabel jlBLZ;
	private JLabel jlKontonummer;
	private JLabel jlBIC;
	private JLabel jlIBAN;
	private JTextField jtfBLZ;
	private JTextField jtfKontonummer;
	private JTextField jtfBIC;
	private JTextField jtfIBAN;
	private JButton jbSpeichern;
	private JLabel jlAusgabe;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Speichern_Konto().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Konto speichern");
		frame.setLayout(null);
		frame.setBounds(600, 300, 450, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		jlBLZ = new JLabel();
		jlBLZ.setBounds(20, 20, 100, 20);
		jlBLZ.setText("Bankleitzahl: ");
		frame.add(jlBLZ);
		
		jlKontonummer = new JLabel();
		jlKontonummer.setBounds(20, 60, 100, 20);
		jlKontonummer.setText("Kontonummer: ");
		frame.add(jlKontonummer);
		
		jlBIC = new JLabel();
		jlBIC.setBounds(20, 100, 100, 20);
		jlBIC.setText("BIC: ");
		frame.add(jlBIC);
		
		jlIBAN = new JLabel();
		jlIBAN.setBounds(20, 140, 100, 20);
		jlIBAN.setText("IBAN: ");
		frame.add(jlIBAN);
		
		jtfBLZ = new JTextField();
		jtfBLZ.setBounds(120, 20, 200, 20);
		frame.add(jtfBLZ);
		
		jtfKontonummer = new JTextField();
		jtfKontonummer.setBounds(120, 60, 200, 20);
		frame.add(jtfKontonummer);
		
		jtfBIC = new JTextField();
		jtfBIC.setBounds(120, 100, 200, 20);
		frame.add(jtfBIC);
		
		jtfIBAN = new JTextField();
		jtfIBAN.setBounds(120, 140, 200, 20);
		frame.add(jtfIBAN);
		
		jbSpeichern = new JButton();
		jbSpeichern.addActionListener(this);
		jbSpeichern.setText("Speichern");
		jbSpeichern.setBounds(20, 180, 300, 25);
		frame.add(jbSpeichern);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 220, 200, 20);
		jlAusgabe.setText("");
		frame.add(jlAusgabe);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		int iBLZ = Integer.parseInt(jtfBLZ.getText());
		int iKontonummer = Integer.parseInt(jtfKontonummer.getText());
		String sBIC = jtfBIC.getText();
		String sIBAN = jtfIBAN.getText();
		
		meineDBV = new Datenbankverbindung();
		boolean bHatgeklappt = meineDBV.speichereKonto(iBLZ, iKontonummer, sBIC, sIBAN);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern");
		}
		
	}

}
