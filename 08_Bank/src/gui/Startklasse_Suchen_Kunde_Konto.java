package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Konto;
import fachklasse.Kunde;

public class Startklasse_Suchen_Kunde_Konto implements ActionListener
{
	private JLabel jlName;
	private JTextField jtfName;
	private JLabel jlGefundeneKunden;
	private JComboBox<String> jcbKunde;
	private JButton jbKundenSuchen;
	private JButton jbKontoSuchen;
	private JTextArea jtaKonto;
	private JScrollPane jspKonto;
	private Datenbankverbindung meineDBV;
	private ArrayList<Kunde> meineKundenListe;

	public static void main(String[] args)
	{
		new Startklasse_Suchen_Kunde_Konto().go();

	}

	private void go()
	{
		JFrame frame = new JFrame();
		frame.setBounds(400, 300, 420, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Kundenkonto suchen");
		
		jlName = new JLabel();
		jlName.setBounds(20, 20, 150, 20);
		jlName.setText("Vorname / Nachname: ");
		frame.add(jlName);
		
		jtfName = new JTextField();
		jtfName.setBounds(160, 20, 150, 20);
		frame.add(jtfName);
		
		jbKundenSuchen = new JButton();
		jbKundenSuchen.setText("Kunden suchen");
		jbKundenSuchen.addActionListener(this);
		jbKundenSuchen.setActionCommand("suchekunden");
		jbKundenSuchen.setBounds(20, 60, 180, 25);
		frame.add(jbKundenSuchen);
		
		jlGefundeneKunden = new JLabel();
		jlGefundeneKunden.setBounds(20, 100, 200, 20);
		jlGefundeneKunden.setText("Gefundene Kunden: ");
		frame.add(jlGefundeneKunden);
		
		jcbKunde = new JComboBox<>();
		jcbKunde.setBounds(160, 100, 200, 20);
		jcbKunde.addItem("Keinen Kunden gefunden");
		frame.add(jcbKunde);
		
		jbKontoSuchen = new JButton();
		jbKontoSuchen.setText("Konto suchen");
		jbKontoSuchen.addActionListener(this);
		jbKontoSuchen.setActionCommand("suchekonto");
		jbKontoSuchen.setBounds(20, 140, 180, 25);
		frame.add(jbKontoSuchen);
		
		jtaKonto = new JTextArea();
		jspKonto = new JScrollPane();
		jspKonto.setBounds(20, 180, 320, 210);
		jspKonto.setViewportView(jtaKonto);
		frame.add(jspKonto);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sActionCommand = e.getActionCommand();
		
		meineDBV = new Datenbankverbindung();
		
		if (sActionCommand == "suchekunden")
		{
			ArrayList<Kunde> meineKundenListe = meineDBV.sucheKundePerName(jtfName.getText());
			for (int i = 0; i < meineKundenListe.size(); i++)
			{
				Kunde meinKunde = meineKundenListe.get(i);
				jcbKunde.addItem(meinKunde.getInr()+" "+meinKunde.getsVorname()+" "+meinKunde.getsNachname()+" "+meinKunde.getsGeburtsdatum()+" "+meinKunde.getsGeburtsort());
			}
			
		}
		
		if (sActionCommand == "suchekonto")
		{
			int iZeileKunde = jcbKunde.getSelectedIndex();
			Kunde meinKunde = meineKundenListe.get(iZeileKunde - 1);
			
			int iKontonummer = meineDBV.sucheKontoNummer(meinKunde.getInr());
			
			ArrayList<Konto> meineKontoListe = new ArrayList<>();
			meineKontoListe = meineDBV.sucheKontoDatenPerKontonummer(iKontonummer);
			jtaKonto.setText("Verkn�pfte Konten: \n");
			
			for (int i = 0; i < meineKundenListe.size(); i++)
			{
				Konto meinKonto = meineKontoListe.get(i);
				jtaKonto.append("BLZ: " + meinKonto.getiBLZ() + " | " + "Kontonummer: " + meinKonto.getiKontonummer() + " | " + "BIC: " + meinKonto.getsBIC() + " | " + "IBAN: " + meinKonto.getsIBAN());				
			}
			
		}
		
	}

}
