package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Konto;
import fachklasse.Kunde;

public class Startklasse_V_Kunde_Konto_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlKunde;
	private JLabel jlKonto;
	private JLabel jlAusgabe;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;
	private JComboBox<String> meineJCBKunde;
	private JComboBox<String> meineJCBKonto;
	private ArrayList<Kunde> meineKundenListe;
	private ArrayList<Konto> meineKontoListe;
	
	public static void main(String[] args)
	{
		new Startklasse_V_Kunde_Konto_speichern().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Buch und Autor verkn�pfen");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlKunde = new JLabel();
		jlKunde.setBounds(20, 20, 200, 20);
		jlKunde.setText("Kunde: ");
		frame.add(jlKunde);
		
		jlKonto = new JLabel();
		jlKonto.setBounds(20, 60, 200, 20);
		jlKonto.setText("Konto: ");
		frame.add(jlKonto);
		
		meineJCBKunde = new JComboBox<>();
		meineJCBKunde.setBounds(120, 20, 200, 20);
		meineJCBKunde.addItem("Bitte einen Kunden ausw�hlen");
		frame.add(meineJCBKunde);
		
		meineJCBKonto = new JComboBox<>();
		meineJCBKonto.setBounds(120, 60, 200, 20);
		meineJCBKonto.addItem("Bitte ein Konto ausw�hlen");
		frame.add(meineJCBKonto);
		
		//************************************************
		// ComboBox f�llen
		meineDBV = new Datenbankverbindung();
		meineKundenListe = meineDBV.sucheAlleKunden();
		for (int i = 0; i < meineKundenListe.size(); i++)
		{
			Kunde meinKunde = meineKundenListe.get(i);
			meineJCBKunde.addItem(meinKunde.getInr()+" "+meinKunde.getsVorname()+" "+meinKunde.getsNachname()+" "+meinKunde.getsGeburtsdatum()+" "+meinKunde.getsGeburtsort());
		}

		
		meineKontoListe = meineDBV.sucheAlleKonten();
		for (int i = 0; i < meineKontoListe.size(); i++)
		{
			Konto meinKonto = meineKontoListe.get(i);
			meineJCBKonto.addItem(meinKonto.getiBLZ()+" "+meinKonto.getiKontonummer()+" "+meinKonto.getsBIC()+" "+meinKonto.getsIBAN());
		}
		
		
		//************************************************
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 140, 270, 26);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// ComboBox Person auslesen
		int iZeileKunde = meineJCBKunde.getSelectedIndex();
		Kunde meinKunde = meineKundenListe.get(iZeileKunde-1);
		
		// ComboBox KFZ auslesen
		int iZeileKonto = meineJCBKonto.getSelectedIndex();
		Konto meinKonto = meineKontoListe.get(iZeileKonto-1);
		
		// Die beiden Zahlen (KFZ-Nr und Personen-Nr) in die Datenbank speichern
		boolean bHatGeklappt = meineDBV.speichere_V_Kunde_Konto(meinKunde.getInr(), meinKonto.getiKontonummer());
		
		if (bHatGeklappt == true)
		{
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Speichern fehlgeschlagen");
		}
		
	}

}
