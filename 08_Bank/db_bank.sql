DROP DATABASE IF EXISTS db_bank;
CREATE DATABASE db_bank;
USE db_bank;

CREATE TABLE kunde (nr INTEGER AUTO_INCREMENT, vorname VARCHAR(30),
nachname VARCHAR(30), geburtsdatum VARCHAR(10), geburtsort VARCHAR(30), PRIMARY KEY (nr))ENGINE = INNODB;

CREATE TABLE konto (blz INTEGER(8),
kontonummer INTEGER(10), bic VARCHAR(11), iban VARCHAR (22),
PRIMARY KEY (kontonummer)) ENGINE = INNODB;

CREATE TABLE v_kunde_konto (nr INTEGER AUTO_INCREMENT, 
kunde_nr INTEGER, konto_kontonummer INTEGER,
FOREIGN KEY (kunde_nr) REFERENCES kunde(nr),
FOREIGN KEY (konto_kontonummer) REFERENCES konto(kontonummer),
PRIMARY KEY (nr))ENGINE=InnoDB;