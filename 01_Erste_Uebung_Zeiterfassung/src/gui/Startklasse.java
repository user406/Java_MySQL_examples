package gui;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import fachklasse.TimerLabel;

public class Startklasse implements ActionListener
{
	private JButton button1;
	private ImageIcon iiStart;
	private ImageIcon iiStopp;
	private TimerLabel timlab;
	private JLabel label;
	long lGesamtzeit = 0;

	public static void main(String[] args)
	{	
		new Startklasse().go();
	}
	
	// Die Methode, die beim Start ausgef�hrt wird
	private void go()
	{
		iiStart = new ImageIcon("img/start.png");
		iiStopp = new ImageIcon("img/stopp.png");
		
		Image image1 = iiStart.getImage();
		image1 = image1.getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH);
		iiStart = new ImageIcon(image1);
		
		Image image2 = iiStopp.getImage();
		image2 = image2.getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH);
		iiStopp = new ImageIcon(image2);
		
		JFrame frame = new JFrame();
		frame.setTitle("Zeiterfassung");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 500);
		frame.setLocation(600, 200);
		frame.setLayout(null);
		
		button1 = new JButton();
		button1.setBounds(20, 20, 200, 200);
		button1.addActionListener(this);
		button1.setIcon(iiStart);
		frame.add(button1);
		
		timlab = new TimerLabel();
		timlab.setBounds(20, 250, 100, 20);
		timlab.setText("---");
		frame.add(timlab);
		
		label = new JLabel();
		label.setBounds(20, 300, 200, 20);
		label.setText("Gesamtzeit: 0 ms");
		frame.add(label);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		JButton buttongeradegeklickt = (JButton) e.getSource();
				
		// TODO Auto-generated method stub
		if (buttongeradegeklickt.getIcon() == iiStart)
		{
			// Uhr steht, Startbutton wird gezeigt
			// Jetzt Uhr laufen lassen und stopbutton wieder anzeigen
			button1.setIcon(iiStopp);
			timlab.starten();
		}
		else
		{
			// Uhr l�uft, Stopbutton wird angezeigt
			// Jetzt die Uhr anhalten und den Startbutton anzeigen
			button1.setIcon(iiStart);
			timlab.stoppen();	
			lGesamtzeit = lGesamtzeit + timlab.getDauerInMs();
			label.setText("Gesamtzeit: "+lGesamtzeit+""+" ms");
		}
		
	}

}
