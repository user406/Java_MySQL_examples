package fachklasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;

	// Methode, die sich mit der Datenbank verbindet
	private void verbindeMitMySQL()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			System.out.println("Drivermanager geladen, Treibername ok");

			try
			{
				meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_highscore_ganovenjagd?user=root&password=");
				meinStatement = meineConnection.createStatement();
				System.out.println("Connection etabliert");

			}
			catch (SQLException eSQL)
			{
				System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
				System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
				System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
			}

		}
		catch (Exception e)
		{
			System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
			System.out.println("Treibername falsch - VendorError: \n" + e.toString());
		}
	}

	// Methode, die die MySQL-Verbindung beendet
	private void beendeMySQLVerbindung()
	{
		try
		{
			meinStatement.close();
			meineConnection.close();
			System.out.println("Verbindung beendet");
		}
		catch (SQLException e)
		{
			System.out.println("Fehler beim Beenden der Verbindung :\n" + e);
		}
	}

	// Methode, die Daten aus der Datenbank ausliest
	private ResultSet leseDaten(String sSQLAnweisung)
	{
		ResultSet meinRS = null;
		try
		{
			meinRS = meinStatement.executeQuery(sSQLAnweisung);
			System.out.println("Leseanfrage ausgef�hrt: " + sSQLAnweisung);
		}
		catch (SQLException eSQL)
		{
			System.out.println("Fehler bei der Leseanfrage: \n" + eSQL);
			meinRS = null;
		}
		return meinRS;
	}

	// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der
	// ver�nderten Zeilen in der Datenbank zur�ck
	private int schreibeDaten(String sSQLAnweisung)
	{
		int iZeilen = 0;
		try
		{
			iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
			System.out.println("Schreibbefehl ausgef�hrt: " + sSQLAnweisung);
		}
		catch (SQLException eSQL)
		{
			System.out.println("Fehler beim Schreibbefehl: \n" + eSQL);
			System.out.println("\n " + sSQLAnweisung);
		}
		return iZeilen;
	}
	//********************************************************************************************
	
	// Eine Methode, die einen Spieler (Vorname, Zeit) in die Datenbank in die 
	// Tabelle scores speichert und zur�ckermittelt, ob es geklappt hat.
	public boolean speichereSpieler (String sVorname, int iZeit)
	{
		boolean bHatGeklappt = false;
		String sSQLAnweisung = "INSERT INTO scores (vorname, zeit) VALUES ('"+sVorname+"', "+iZeit+");";

		try
		{
			this.verbindeMitMySQL();
			int iZeilen = this.schreibeDaten(sSQLAnweisung);
			if (iZeilen == 1)
			{
				bHatGeklappt = true;
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Speichern des Spielers.\n" + e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return bHatGeklappt;
	}
	//*******************************************************************************************
	
	// Eine Methode, die eine Liste aller Spieler (Nr, Vorname, Zeit) aus der Datenbank
	// aus der Tabelle scores sucht und ausgibt - sortiert nach Zeit, beste zeit zuerst
	public ArrayList<Spieler> sucheAlleSpielerSortiertNachZeit ()
	{
		ArrayList<Spieler> meineSpielerliste = new ArrayList<Spieler>();
		
		String sSQLAnweisung = "SELECT nr, vorname, zeit FROM scores ORDER BY (zeit) ASC;";
		
		try
		{
			this.verbindeMitMySQL();
			
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			
			while (meinRS.next() == true)
			{
				Spieler meinSpieler = new Spieler();
				meinSpieler.setiNummer(meinRS.getInt("nr"));
				meinSpieler.setsVorname(meinRS.getString("vorname"));
				meinSpieler.setiZeit(meinRS.getInt("zeit"));
				meineSpielerliste.add(meinSpieler);
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen aller Spieler sortiert nach Zeit.\n" + e.toString());
		}
		finally 
		{
			this.beendeMySQLVerbindung();
		}
		
		return meineSpielerliste;
	}
	//*******************************************************************************************

	// Eine Methode, die den besten Spieler mit der besten (also kleinste) Zeit aus der 
	// Datenbank aus der Tabelle scores sucht und zur�ckgibt
	public Spieler sucheSpielerMitBesterZeit ()
	{
		Spieler meinSpieler = null;
		
		String sSQLAnweisung = "SELECT nr, vorname, zeit FROM scores WHERE zeit = (SELECT MIN(zeit) FROM scores);";
		
		try
		{	
			this.verbindeMitMySQL();
			ResultSet meinRS = this.leseDaten(sSQLAnweisung);
			if (meinRS.next() == true)
			{
				meinSpieler = new Spieler();
				meinSpieler.setiNummer(meinRS.getInt("nr"));
				meinSpieler.setsVorname(meinRS.getString("vorname"));
				meinSpieler.setiZeit(meinRS.getInt("zeit"));
			}
		}
		catch (Exception e)
		{
			System.out.println("Fehler beim Suchen des Spielers mit der besten Zeit.\n" + e.toString());
		}
		finally
		{
			this.beendeMySQLVerbindung();
		}
		
		return meinSpieler;
	}
	//********************************************************************************************
	
	// Methode, die alle Eintr�ge l�scht
		public boolean loescheHighscores ()
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung = "DELETE FROM scores WHERE nr > 0;";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen >= 0)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim L�schen der Eintr�ge. \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		//***************************************************************************************************
		
		// Methode, die automatisch hochz�hlenden Prim�rschl�ssel
		// wieder von vorne z�hlen l�sst
		public boolean setzeNrAutoIncrementZur�ck ()
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung = "ALTER TABLE scores AUTO_INCREMENT = 1;";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 0)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim L�schen der Eintr�ge. \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		
}
