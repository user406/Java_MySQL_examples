package fachklasse;

public class Spieler
{
	private int iNummer;
	private String sVorname;
	private int iZeit;
	
	public int getiNummer()
	{
		return iNummer;
	}
	public void setiNummer(int iNummer)
	{
		this.iNummer = iNummer;
	}
	public String getsVorname()
	{
		return sVorname;
	}
	public void setsVorname(String sVorname)
	{
		this.sVorname = sVorname;
	}
	public int getiZeit()
	{
		return iZeit;
	}
	public void setiZeit(int iZeit)
	{
		this.iZeit = iZeit;
	}
}
