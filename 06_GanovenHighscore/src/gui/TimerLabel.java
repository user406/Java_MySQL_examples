package gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.Timer;

public class TimerLabel extends JLabel {
	
	private static Timer timer;
	private long duration;
	private static boolean go;
	
	public void starten(){
		final long now = System.currentTimeMillis();
		timer = new Timer(10, new ActionListener() {
			double i = 0;
		    public void actionPerformed(ActionEvent e) {
		        i++;
		        duration = System.currentTimeMillis()-now;
		        setText(getDauerAsString());
		    }
		});
		timer.setRepeats(true);
		timer.start();		
	}
	
	public void stoppen(){
		if(timer != null){
			timer.stop();
		}
		timer = null;
	}
	
	public void zuruecksetzen(){
		stoppen();
		setText("0.0");
	}
	
	public long getDauerInMs(){
		return duration;
	}
	
	private String getDauerAsString(){
		String m = "";
		String s = "";
		String ms = "";
		
		long minutes = Math.abs(duration/60000);
		if(minutes < 10) {
			m = "0"+minutes;
		} else {
			m = ""+minutes;
		}
		long seconds = (duration - minutes*60000)/1000;
		if(seconds < 10) {
			s = "0"+seconds;
		} else {
			s = ""+seconds;
		}
		long milliseconds = (duration - minutes*60000 - seconds*1000);
		if(milliseconds < 10) {
			ms = "00"+milliseconds;
		} else if(milliseconds < 100) {
			ms = "0"+milliseconds;
		} else {
			ms = ""+milliseconds;
		}
		return m + ":" + s + ":" + ms;
	}

}
