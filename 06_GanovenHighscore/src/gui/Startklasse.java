package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fachklasse.Datenbankverbindung;
import fachklasse.Spieler;


public class Startklasse implements ActionListener
{

	private ImageIcon meinIconRichtig;
	private ImageIcon meinIconFalsch;
	private ImageIcon meinIconRaeuber;
	private ImageIcon meinIconPolizei;
	private TimerLabel meinTimLab;
	private JLabel meinJLAusgabe;
	private JFrame meinFrame;
	private JOptionPane meinOPMeldung;
	private JLabel meinJLHighscore;
	private JTextArea meineJTAHighscorelist;
	private JScrollPane meinJSPHighscore;
	private int iAnzahlRaeuberImSpiel = 0;
	private int iAktuelleBestzeit = 0;
	private int iSekunde = 0;
	private JButton meinJBLoeschen;
	
	
	public static void main(String[] args)
	{
		(new Startklasse()).go();
	}

	private void go()
	{
		meinIconRichtig = new ImageIcon("img/richtig.png");
		meinIconFalsch = new ImageIcon("img/falsch.png");

		meinFrame = new JFrame();
		meinFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meinFrame.setSize(1100, 780);
		meinFrame.setTitle("R�ubersuche");
		meinFrame.setLayout(null);

		JButton meinButton;

		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 6; j++)
			{
				// Zufallsvariable, die bestimmt ob es ein R�uber oder Polizei
				// ist
				// 0=R�uber, 1=Polizei
				int iR_oder_P = (int) (Math.random() * 2);

				meinButton = new JButton();
				meinButton.setBounds(10 + i * 110, 10 + j * 110, 100, 100);

				// Zufallsvariable von 1 bis 9, die das Bild bestimmt
				int iZufallsBild = (int) (Math.random() * 9) + 1;

				if (iR_oder_P == 0)
				{
					// R�uber
					meinIconRaeuber = new ImageIcon("img/raeuber" + iZufallsBild + ".png");
					meinButton.setIcon(meinIconRaeuber);
					meinButton.setActionCommand("raeuber");
					iAnzahlRaeuberImSpiel = iAnzahlRaeuberImSpiel + 1;
				}
				else
				{
					// Polizei
					meinIconPolizei = new ImageIcon("img/polizei" + iZufallsBild + ".png");
					meinButton.setIcon(meinIconPolizei);
					meinButton.setActionCommand("polizei");
				}
				meinButton.addActionListener(this);
				meinFrame.add(meinButton);
			}
		}

		meinJBLoeschen = new JButton();
		meinJBLoeschen.setBounds(850, 430, 170, 20);
		meinJBLoeschen.addActionListener(this);
		meinJBLoeschen.setActionCommand("loeschen");
		meinJBLoeschen.setText("Eintr�ge l�schen");
		meinFrame.add(meinJBLoeschen);
		
		meinJLAusgabe = new JLabel();
		meinJLAusgabe.setBounds(20, 680, 300, 20);
		meinJLAusgabe.setText("R�uber auf freiem Fu�: " + iAnzahlRaeuberImSpiel);
		meinFrame.add(meinJLAusgabe);

		// Teil, in dem die Highscoretabelle angelegt wird
		// �berschrift
		meinJLHighscore = new JLabel();
		meinJLHighscore.setBounds(700, 50, 300, 20);
		meinJLHighscore.setText("Highscore:");
		meinFrame.add(meinJLHighscore);
		
		// TextArea
		meineJTAHighscorelist = new JTextArea();
		meineJTAHighscorelist.setEditable(true);
		
		// Scrollpane f�r Scrollbalken
		meinJSPHighscore = new JScrollPane();
		meinJSPHighscore.setBounds(700, 90, 320, 320);
		meinJSPHighscore.setViewportView(meineJTAHighscorelist);
		meinFrame.add(meinJSPHighscore);
		
		// Highscore f�llen
		
		// **************************************************
		
		
		
		
		
		
		
		// **************************************************

		// TimerLabel
		meinTimLab = new TimerLabel();
		meinTimLab.setBounds(350, 680, 200, 20);
		meinTimLab.setText("0");
		meinFrame.add(meinTimLab);
		meinTimLab.starten();

		meinFrame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e)
	{
		String sActionCommand = e.getActionCommand();
		JButton meinButtonGeklickt = (JButton) e.getSource();

		// �berpr�fung ob R�uber oder Polizist angeklickt wurde
		
		if (sActionCommand.equals("raeuber"))	// R�uber wurde angeklickt
		{
			meinButtonGeklickt.setIcon(meinIconRichtig);
			meinButtonGeklickt.removeActionListener(this);
			iAnzahlRaeuberImSpiel = iAnzahlRaeuberImSpiel - 1;
			meinJLAusgabe.setText("R�uber auf freiem Fu�: " + iAnzahlRaeuberImSpiel);

			if (iAnzahlRaeuberImSpiel == 0)		// Alle R�uber wurden erwischt
			{
				meinTimLab.stoppen();
						
				// ****************************************	
				
				
				
				
				//*****************************************
			}
		}
		
		if(sActionCommand.equals("polizei"))	// Polizist wurde angeklickt
		{
			meinButtonGeklickt.setIcon(meinIconFalsch);
			meinButtonGeklickt.removeActionListener(this);
		}
				
		// Wenn L�schen Button geklickt wird, wird Textdatei 
		// mit den Highscores gel�scht
		if (sActionCommand.equals("loeschen"))
		{
			File fHighscores = new File("highscores.txt");
					
			// Wenn Textdatei mit dem Namen highscores.txt existiert,
			// wird diese aus Workspace gel�scht
			if(fHighscores.exists())
			{
				fHighscores.delete();
				meineJTAHighscorelist.setFont(meineJTAHighscorelist.getFont().deriveFont(Font.BOLD));
				meineJTAHighscorelist.setText("Keine Eintr�ge gefunden.");
			}
					
			System.out.println("Datei erfolgreich gel�scht");
		}
		
		
	}
}
