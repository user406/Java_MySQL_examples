package fachklasse;

public class Spieler
{
	private int iNummer;
	private String sVornanme;
	private int iZeit;
	
	public int getiNummer()
	{
		return iNummer;
	}
	public void setiNummer(int iNummer)
	{
		this.iNummer = iNummer;
	}
	public String getsVornanme()
	{
		return sVornanme;
	}
	public void setsVornanme(String sVornanme)
	{
		this.sVornanme = sVornanme;
	}
	public int getiZeit()
	{
		return iZeit;
	}
	public void setiZeit(int iZeit)
	{
		this.iZeit = iZeit;
	}

}
