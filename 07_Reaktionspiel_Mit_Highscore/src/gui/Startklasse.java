package gui;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.PasswordAuthentication;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Spieler;
import fachklasse.TimerLabel;

public class Startklasse implements ActionListener
{

	private ImageIcon meinIIRichtig;
	private ImageIcon meinIIFalsch;
	private ImageIcon meinIIPolizei;
	private ImageIcon meinIIRaeuber;
	private JButton meinJB1;
	private JButton meinJB2;
	private JButton meinJB3;
	private JButton meinJB4;
	private JButton meinJBStart;
	private JButton meinJBLoeschen;
	private JButton meinJBSpeichern;
	private JTextArea meineJTAHighscore;
	private JScrollPane meinJSPHighscore;
	JFrame meinFrame;
	private TimerLabel meinTimLab;
	private JOptionPane meinOPMeldung;
	
	public static void main(String[] args)
	{
		(new Startklasse()).go();
	}

	private void go()
	{
		meinIIRichtig = new ImageIcon("img/richtig.png");
		meinIIFalsch = new ImageIcon("img/falsch.png");
		meinIIRaeuber = new ImageIcon("img/raeuber.png");
		meinIIPolizei = new ImageIcon("img/polizei.png");

		meinFrame = new JFrame();
		meinFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meinFrame.setSize(750, 550);
		meinFrame.setTitle("Reaktionstest");
		meinFrame.setLayout(null);
		
		meinJB1 = new JButton();
		meinJB1.setBounds(20, 20, 100, 100);
		meinFrame.add(meinJB1);
		
		meinJB2 = new JButton();
		meinJB2.setBounds(260, 20, 100, 100);
		meinFrame.add(meinJB2);
		
		meinJB3 = new JButton();
		meinJB3.setBounds(20, 260, 100, 100);
		meinFrame.add(meinJB3);
		
		meinJB4 = new JButton();
		meinJB4.setBounds(260, 260, 100, 100);
		meinFrame.add(meinJB4);
		
		meinJBStart = new JButton();
		meinJBStart.setBounds(140, 140, 100, 100);
		meinJBStart.addActionListener(this);
		meinJBStart.setText("START");
		meinFrame.add(meinJBStart);
		
		meinJBLoeschen = new JButton();
		meinJBLoeschen.setBounds(500, 380, 170, 20);
		meinJBLoeschen.addActionListener(this);
		meinJBLoeschen.setActionCommand("loeschen");
		meinJBLoeschen.setText("Eintr�ge l�schen");
		meinFrame.add(meinJBLoeschen);
		
		meinJBSpeichern = new JButton();
		meinJBSpeichern.setBounds(500, 420, 170, 20);
		meinJBSpeichern.addActionListener(this);
		meinJBSpeichern.setActionCommand("exportieren");
		meinJBSpeichern.setText("Highscores exportieren");
		meinFrame.add(meinJBSpeichern);
		
		

		JLabel meinJL1 = new JLabel();
		meinJL1.setBounds(20, 400, 400, 20);
		meinJL1.setText("Nach Dr�cken des Startbuttons m�ssen Sie so schnell wie");
		meinFrame.add(meinJL1);
		
		JLabel meinJL2 = new JLabel();
		meinJL2.setBounds(20, 420, 400, 20);
		meinJL2.setText("m�glich auf den R�uber klicken.");
		meinFrame.add(meinJL2);
		
		JLabel meinJLHighscore = new JLabel();
		meinJLHighscore.setBounds(420, 30, 100, 20);
		meinJLHighscore.setText("Highscores:");
		meinFrame.add(meinJLHighscore);
		
		// TextArea erstellen, in der die Highscores angezeigt werden
		meineJTAHighscore = new JTextArea();
		
		// Scrollpane f�r Scrollbalken
		meinJSPHighscore = new JScrollPane();
		meinJSPHighscore.setBounds(420, 60, 250, 300);
		meinJSPHighscore.setViewportView(meineJTAHighscore);
		meinFrame.add(meinJSPHighscore);
		
		// Timerlabel
		meinTimLab = new TimerLabel();
		meinTimLab.setBounds(20, 450, 150, 20);
		meinTimLab.setText("Zeit: 0 Millisekunden");
		meinFrame.add(meinTimLab);
		
		// Highscore f�llen
		Datenbankverbindung meineDBV = new Datenbankverbindung();
				
		// Spielerliste aus Datenbank auslesen und 
		// Textarea mit aktuellen Highscores f�llen
		ArrayList<Spieler> meineSpielerListe = meineDBV.sucheAlleSpielerSortiertNachZeit();
		if (meineSpielerListe.size() == 0)
		{
			meineJTAHighscore.setText("Keinen Spieler gefunden");
		}
		else
		{
			for (int i = 0; i < meineSpielerListe.size(); i++)
			{	
				int iPlatz = i + 1;
				Spieler meinSpieler = meineSpielerListe.get(i);
				meineJTAHighscore.append("Platz"+" "+iPlatz+":  "+meinSpieler.getsVornanme() +" "+ meinSpieler.getiZeit() +" ms\n");
			}
		}
		
		meinFrame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		JButton meinButtonGeradeGeklickt = (JButton) e.getSource();
		String sActionCommand = e.getActionCommand();
		
		if (meinButtonGeradeGeklickt == meinJBStart)
		{
			// Erstmaliger Klick auf Startbutton
			meinTimLab.starten();
			meinJBStart.removeActionListener(this);
			int iZufall = (int) (Math.random()*4)+1;
			
			switch (iZufall)
			{
			case 1:
				meinJB1.setIcon(meinIIRaeuber);
				meinJB2.setIcon(meinIIPolizei);
				meinJB3.setIcon(meinIIPolizei);
				meinJB4.setIcon(meinIIPolizei);
				break;
			
			case 2:
				meinJB1.setIcon(meinIIPolizei);
				meinJB2.setIcon(meinIIRaeuber);
				meinJB3.setIcon(meinIIPolizei);
				meinJB4.setIcon(meinIIPolizei);
				break;

			case 3:
				meinJB1.setIcon(meinIIPolizei);
				meinJB2.setIcon(meinIIPolizei);
				meinJB3.setIcon(meinIIRaeuber);
				meinJB4.setIcon(meinIIPolizei);
				break;
				
			case 4:
				meinJB1.setIcon(meinIIPolizei);
				meinJB2.setIcon(meinIIPolizei);
				meinJB3.setIcon(meinIIPolizei);
				meinJB4.setIcon(meinIIRaeuber);
				break;
			default:
				break;
			}
			
			// Den vier Buttons den Actionlistener hinzuf�gen
			meinJB1.addActionListener(this);
			meinJB2.addActionListener(this);
			meinJB3.addActionListener(this);
			meinJB4.addActionListener(this);
		}
		else
		{
			
			if (meinButtonGeradeGeklickt.getIcon() == meinIIRaeuber)
			{
				meinButtonGeradeGeklickt.setIcon(meinIIRichtig);
				meinButtonGeradeGeklickt.removeActionListener(this);
				meinTimLab.stoppen();
				int iMillisek = (int) meinTimLab.getDauerInMs();
				meinOPMeldung = new JOptionPane();
				meinOPMeldung.showMessageDialog(meinFrame, "Geschafft in "+iMillisek+" Millisekunden.");
				
				Datenbankverbindung meineDBV = new Datenbankverbindung();
				Spieler meinSpieler = meineDBV.sucheBestenSpieler();
				
				if (meinSpieler == null)	// �berpr�fung, ob es einen besten Spieler gibt
				{
					// Es gibt keinen besten Spieler
					String sVorname = meinOPMeldung.showInputDialog(meinFrame, "<html><body>Bestzeit: "+" "+iMillisek+" ms"+" "+" <br>Geben Sie Ihren Vornamen ein:</body></html>", "Highscore speichern", JOptionPane.INFORMATION_MESSAGE);
					
					boolean bHatGeklappt = meineDBV.speichereSpieler(sVorname, iMillisek);
					if (bHatGeklappt == true)
					{
						meinOPMeldung.showMessageDialog(meinFrame, "Speichern erfolgreich.", "Speichern erfolgreich", JOptionPane.INFORMATION_MESSAGE);
					}
					else
					{
						meinOPMeldung.showMessageDialog(meinFrame, "Fehler beim Speichern.", "Speichern fehlgeschlagen", JOptionPane.ERROR_MESSAGE);
					}
				}
				else
				{
					if (iMillisek < meinSpieler.getiZeit())	// �berpr�fung, ob man schneller ist als der beste Spieler
					{
						// Es gibt einen besten Spieler
						String sVorname = meinOPMeldung.showInputDialog(meinFrame, "<html><body>Bestzeit: "+" "+iMillisek+" ms"+" "+" <br>Geben Sie Ihren Vornamen ein:</body></html>", "Highscore speichern", JOptionPane.INFORMATION_MESSAGE);
						
			
						boolean bHatGeklappt = meineDBV.speichereSpieler(sVorname, iMillisek);
						if (bHatGeklappt == true)
						{
							meinOPMeldung.showMessageDialog(meinFrame, "Speichern erfolgreich.", "Speichern erfolgreich", JOptionPane.INFORMATION_MESSAGE);
						}
						else
						{
							meinOPMeldung.showMessageDialog(meinFrame, "Fehler beim Speichern.", "Speichern fehlgeschlagen", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				Startklasse.main(null);  
				meinFrame.dispose();
				
			}
				
			else
			{
				meinButtonGeradeGeklickt.setIcon(meinIIFalsch);
				meinButtonGeradeGeklickt.removeActionListener(this);
			}
			
			Datenbankverbindung meineDBV = new Datenbankverbindung();
			
			if (meinButtonGeradeGeklickt == meinJBLoeschen && meineJTAHighscore.getText() != "Keinen Spieler gefunden")
			{
				boolean bHatGeklappt1 = meineDBV.loescheHighscores();
				boolean bHatGeklappt2 = meineDBV.setzeNrAutoIncrementZur�ck();
				
				if (bHatGeklappt1 == true && bHatGeklappt2 == true)
				{
					meineJTAHighscore.setText("Keinen Spieler gefunden");
					Startklasse.main(null);
					meinFrame.dispose();
				}
				meinJBLoeschen.setIcon(null);
				
			}
			
			if (sActionCommand == "exportieren")
			{
				String sHighscores = meineJTAHighscore.getText();
				
				// Aktuelles Datum auslesen und in String speichern
				DateFormat dfDatum = new SimpleDateFormat("dd.MM.yyyy");
				String sAktuellesDatum = dfDatum.format(new Date());
				
				// Aktuelle Uhrzeit auslesen und in String speichern
				DateFormat dfUhrzeit = new SimpleDateFormat("HH:mm");
			    String sAktuelleUhrzeit = dfUhrzeit.format(new Date());  
				
				PrintWriter pwHighscoresTXT;
				
				// Highscores als Textdatei (in Eclipse-Workspace) speichern
				try
				{
					pwHighscoresTXT = new PrintWriter(new FileWriter("highscores.txt", true));
					pwHighscoresTXT.print(sHighscores);
					pwHighscoresTXT.println("");
					pwHighscoresTXT.println("");
					pwHighscoresTXT.println("");
					pwHighscoresTXT.println("");
					pwHighscoresTXT.print("Erstellt am "+sAktuellesDatum+","+" um "+sAktuelleUhrzeit+" Uhr");
					pwHighscoresTXT.close();
				}
				
				catch (IOException e1)
				{
					System.out.println("Fehler beim Exportieren der Highscores.\n"+e1.toString());
				}
				
				meinJBSpeichern.setIcon(null);
						
			}
				
		}
		
	
	}
}

