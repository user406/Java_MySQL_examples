DROP DATABASE IF EXISTS db_highscore_reaktionsspiel;
CREATE DATABASE db_highscore_reaktionsspiel;
USE db_highscore_reaktionsspiel;

CREATE TABLE scores (nr INTEGER AUTO_INCREMENT, vorname VARCHAR(30),
zeit INTEGER, PRIMARY KEY (nr))ENGINE = INNODB;