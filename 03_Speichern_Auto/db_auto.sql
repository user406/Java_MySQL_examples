DROP DATABASE IF EXISTS db_auto;
CREATE DATABASE db_auto;
USE  db_auto;

CREATE TABLE auto (nr INTEGER AUTO_INCREMENT, marke VARCHAR(30), 
modell VARCHAR(30), preis DOUBLE, PRIMARY KEY (nr))ENGINE=InnoDB;