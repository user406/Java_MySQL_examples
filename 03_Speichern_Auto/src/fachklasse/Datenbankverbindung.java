package fachklasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;
	
	// Methode, die sich mit der Datenbank verbindet
		private void verbindeMitMySQL()
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				System.out.println("Drivermanager geladen, Treibername ok");
				
				try
				{
					meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_kfz?user=root&password=");
					meinStatement = meineConnection.createStatement();
					System.out.println("Connection etabliert");
					
				}
				catch (SQLException eSQL)
				{
					System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
					System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
					System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
				System.out.println("Treibername falsch - VendorError: \n" + e.toString());
			}
		}
		
		// Methode, die die MySQL-Verbindung beendet
		private void beendeMySQLVerbindung()
		{
			try
			{
				meinStatement.close();
				meineConnection.close();
				System.out.println("Verbindung beendet");
			}
			catch (SQLException e)
			{
				System.out.println("Fehler beim Beenden der Verbindung :\n"+e);
			}
		}

		// Methode, die Daten aus der Datenbank ausliest
		private ResultSet leseDaten (String sSQLAnweisung)
		{
			ResultSet meinRS = null;
			try
			{
				meinRS = meinStatement.executeQuery(sSQLAnweisung);
				System.out.println("Leseanfrage ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler bei der Leseanfrage: \n"+eSQL);
				meinRS = null;
			}
			return meinRS;
		}
		
		// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der ver�nderten Zeilen in der 
		// Datenbank zur�ck
		private int schreibeDaten (String sSQLAnweisung)

		{
			int iZeilen = 0;
			try
			{
				iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
				System.out.println("Schreibbefehl ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler beim Schreibbefehl: \n"+eSQL);
				System.out.println("\n "+sSQLAnweisung);
			}
			return iZeilen;
		}
		//*******************************************************************************************************
		
		public boolean speichereAutoDaten (String sMarke, String sModell, double dPreis)
		{
			boolean bHatgeklappt = false;
			String sSQLAnweisung = "INSERT INTO auto (marke, modell, preis) VALUES ('"+sMarke+"', '"+sModell+"', '"+dPreis+"');";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatgeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler bei der Methode speichereAutoDaten: \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();	
			}
			
			return bHatgeklappt;
		}
		//********************************************************************************************************
		
		public Auto sucheAutoperNummer (int iNummer)
		{
			Auto meinAuto = null;
			
			String sSQLAnweisung = "SELECT marke, modell, preis FROM auto WHERE nr = "+iNummer+";";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				
				if (meinRS.next() == true)
				{
					meinAuto = new Auto();
					meinAuto.setMarke(meinRS.getString("marke"));
					meinAuto.setModell(meinRS.getString("modell"));
					meinAuto.setPreis(meinRS.getDouble("preis"));
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Autos per Nummer\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meinAuto;
		}
		//****************************************************************************************************
		
		public ArrayList<Auto> sucheAutoPerMarke (String sMarke)
		{
			ArrayList<Auto> meineAutoListe = null;
			Auto meinAuto = null;
					
			String sSQLAnweisung = "SELECT nr, marke, modell, preis FROM auto WHERE marke LIKE '%"+sMarke+"%' OR modell LIKE '%"+sMarke+"%';";
					
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
					
				meineAutoListe = new ArrayList<>();
				
				while (meinRS.next() == true)
				{
					meinAuto = new Auto();
					meinAuto.setNummer(meinRS.getInt("nr"));
					meinAuto.setMarke(meinRS.getString("marke"));
					meinAuto.setModell(meinRS.getString("modell"));
					meinAuto.setPreis(meinRS.getDouble("preis"));
					meineAutoListe.add(meinAuto);
				}	
			}
					
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Autos per Marke: \n"+e.toString());
			}
					
			finally
			{
				this.beendeMySQLVerbindung();
			}
					
			return meineAutoListe;
		}
		//****************************************************************************
		public boolean speicherePerson (String sVorname, String sNachname)
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung = "INSERT INTO person (vorname, nachname) VALUES ('"+sVorname+"', '"+sNachname+"');";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Speichern der Person.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		//****************************************************************************************
		
		// Methode, die alle Eintr�ge in der Tabelle Person sucht
				public ArrayList<Person> sucheAllePersonen ()
				{
					ArrayList<Person> meinePersonenListe = new ArrayList<>();
					Person meinePerson = null;
					
					String sSQLAnweisung = "SELECT nr, vorname, nachname FROM person;";
					
					try
					{
						this.verbindeMitMySQL();
						ResultSet meinRS = this.leseDaten(sSQLAnweisung);
						while (meinRS.next() == true)
						{
							meinePerson = new Person();
							meinePerson.setiNr(meinRS.getInt("nr"));
							meinePerson.setsVorname(meinRS.getString("vorname"));
							meinePerson.setsNachname(meinRS.getString("nachname"));
							meinePersonenListe.add(meinePerson);
						}
					}
					catch (Exception e)
					{
						System.out.println("Fehler beim Suchen aller Personen.\n"+e.toString());
					}
					finally
					{
						this.beendeMySQLVerbindung();
					}
					
					return meinePersonenListe;
				}
				//*************************************************************************************
				
				// Methode, die alle Eintr�ge in der Tabelle Auto sucht
				public ArrayList<Auto> sucheAlleAutos ()
				{
					ArrayList<Auto> meineAutoListe = new ArrayList<>();
					Auto meinAuto = null;
					
					String sSQLAnweisung = "SELECT nr, marke, modell, preis FROM auto;";
					
					try
					{
						this.verbindeMitMySQL();
						ResultSet meinRS = this.leseDaten(sSQLAnweisung);
						while (meinRS.next() == true)
						{
							meinAuto = new Auto();
							meinAuto.setNummer(meinRS.getInt("nr"));
							meinAuto.setMarke(meinRS.getString("marke"));
							meinAuto.setModell(meinRS.getString("modell"));
							meinAuto.setPreis(meinRS.getDouble("preis"));
							meineAutoListe.add(meinAuto);
						}
					}
					catch (Exception e)
					{
						System.out.println("Fehler beim Suchen aller Autos.\n"+e.toString());
					}
					finally
					{
						this.beendeMySQLVerbindung();
					}
					
					return meineAutoListe;
				}
				//*************************************************************************************
				
				// Methode, die Person-Nr und Auto-Nr in die 
				// Verkn�pfungstabelle speichert
				public boolean speichere_V_Person_Auto (int iPersonNummer, int iAutoNummer)
				{
					boolean bHatGeklappt = false;
					
					String sSQLAnweisung  = "INSERT INTO v_kfz_person (auto_nr, person_nr) VALUES ("+iAutoNummer+", "+iPersonNummer+");";
					
					try
					{
						this.verbindeMitMySQL();
						int iZeilen = this.schreibeDaten(sSQLAnweisung);
						if (iZeilen == 1)
						{
							bHatGeklappt = true;
						}
					}
					catch (Exception e)
					{
						System.out.println("Fehler beim Speichern in die Verkn�pfungstabelle.\n"+e.toString());
					}
					finally
					{
						this.beendeMySQLVerbindung();
					}
					
					return bHatGeklappt;
				}

}
