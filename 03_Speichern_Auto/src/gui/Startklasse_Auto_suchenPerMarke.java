package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Auto;
import fachklasse.Datenbankverbindung;

public class Startklasse_Auto_suchenPerMarke implements ActionListener
{
	// Dinge ankündigen
	private JFrame frame;
	private JButton jbSuchen;
	private JLabel jlMarke;
	private JTextField jtfMarke;
	private JTextArea jtaAusgabe;
	private JScrollPane jspAusgabe;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Auto_suchenPerMarke().go();
	}

	private void go()
	{
		// Frame erstellen
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Auto suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 350, 300);
	
		// Button, der die Suche in der ActionPerformed Methode startet
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 250, 26);
		frame.add(jbSuchen);
		
		// Label mit  Inhalt "Marke" erstellen
		jlMarke = new JLabel();
		jlMarke.setBounds(20, 20, 100, 20);
		jlMarke.setText("Marke / Modell: ");
		frame.add(jlMarke);
		
		// Textfeld, in welchem die gefundene Marke des Autos erscheint
		jtfMarke = new JTextField();
		jtfMarke.setBounds(120, 20, 150, 20);
		jtfMarke.setEditable(true);
		frame.add(jtfMarke);
		
		jtaAusgabe = new JTextArea();
		jtaAusgabe.setEditable(false);
		jspAusgabe = new JScrollPane();
		jspAusgabe.setBounds(20, 100, 300, 150);
		jspAusgabe.setViewportView(jtaAusgabe);
		frame.add(jspAusgabe);
		
		// Frame erscheinen lassen
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		
		meineDBV = new Datenbankverbindung();
		
		ArrayList<Auto> meineAutoListe = meineDBV.sucheAutoPerMarke(jtfMarke.getText());
		
		if (meineAutoListe.size() == 0)
		{
			jtaAusgabe.setForeground(Color.RED);
			jtaAusgabe.setText("Kein Fahrzeug gefunden.");
		}
		else
		{
			jtaAusgabe.setText("Folgende Fahrzeuge gefunden: \n");
			
			for (int i = 0; i < meineAutoListe.size(); i++)
			{
				Auto meinAuto = meineAutoListe.get(i);
				jtaAusgabe.append("Nr: " + meinAuto.getNummer() +"|" +" "+ "Marke: " + meinAuto.getMarke() +"|" +" "+ "Modell: " + meinAuto.getModell() +"|" +" "+ "Preis: " + meinAuto.getPreis() + "0€\n");
			}
		}
		
		

	}

}
