package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Person_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlVorname;
	private JLabel jlNachname;
	private JLabel jlAusgabe;
	private JTextField jtfVorname;
	private JTextField jtfNachname;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;
	
	public static void main(String[] args)
	{
		new Startklasse_Person_speichern().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Person speichern");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 20, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jlNachname = new JLabel();
		jlNachname.setBounds(20, 60, 100, 20);
		jlNachname.setText("Nachname: ");
		frame.add(jlNachname);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(120, 20, 150, 20);
		frame.add(jtfVorname);
		
		jtfNachname = new JTextField();
		jtfNachname.setBounds(120, 60, 150, 20);
		frame.add(jtfNachname);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 140, 250, 26);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sVorname = jtfVorname.getText();
		String sNachname = jtfNachname.getText();
		
		meineDBV = new Datenbankverbindung();
		
		boolean bHatgeklappt = meineDBV.speicherePerson(sVorname, sNachname);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setForeground(Color.BLACK);
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern");
		}
		
	}

}
