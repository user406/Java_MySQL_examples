package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Auto;
import fachklasse.Datenbankverbindung;

public class Startklasse_Auto_suchenPerNummer implements ActionListener
{
	// Dinge ankündigen
	private JFrame frame;
	private JLabel jlNR;
	private JButton jbSuchen;
	private JLabel jlMarke;
	private JLabel jlModell;
	private JLabel jlPreis;
	private JTextField jtfMarke;
	private JTextField jtfModell;
	private JTextField jtfNR;
	private JTextField jtfPreis;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Auto_suchenPerNummer().go();

	}

	private void go()
	{
		// Frame erstellen
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Auto suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 350, 300);
	
		// Label mit Inhalt "Nummer" erstellen
		jlNR = new JLabel();
		jlNR.setBounds(20, 20, 100, 20);
		jlNR.setText("Nr: ");
		frame.add(jlNR);
		
		// Textfeld, in welches die zu suchende Nummer eingetragen wird
		jtfNR = new JTextField();
		jtfNR.setBounds(100, 20, 150, 20);
		frame.add(jtfNR);
		
		// Button, der die Suche in der ActionPerformed Methode startet
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 230, 26);
		frame.add(jbSuchen);
		
		// Label mit  Inhalt "Marke" erstellen
		jlMarke = new JLabel();
		jlMarke.setBounds(20, 120, 100, 20);
		jlMarke.setText("Vorname: ");
		frame.add(jlMarke);
		
		// Label mit Inhalt "Modell" erstellen
		jlModell = new JLabel();
		jlModell.setBounds(20, 160, 100, 20);
		jlModell.setText("Nachname: ");
		frame.add(jlModell);
		
		// Textfeld, in welchem die gefundene Marke des Autos erscheint
		jtfMarke = new JTextField();
		jtfMarke.setBounds(100, 120, 150, 20);
		jtfMarke.setEditable(false);
		frame.add(jtfMarke);
		
		// Textfeld, in welchem das gefundene Modell des Autos erscheint
		jtfModell = new JTextField();
		jtfModell.setBounds(100, 160, 150, 20);
		jtfModell.setEditable(false);
		frame.add(jtfModell);
		
		// Label mit Inhalt "Preis" erstellen
		jlPreis = new JLabel();
		jlPreis.setBounds(20, 200, 100, 20);
		jlPreis.setText("Preis: ");
		frame.add(jlPreis);
		
		// Textfeld, in welchem der gefundene Preis des Autos erscheint
		jtfPreis = new JTextField();
		jtfPreis.setBounds(100, 200, 150, 20);
		jtfPreis.setEditable(false);
		frame.add(jtfPreis);
		
		// Frame erscheinen lassen
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// Neue Datenbankverbindung anlegen
		meineDBV = new Datenbankverbindung();
		
		// Eingegebene Nummer auslesen und in Integer umwandeln
		String sNummer = jtfNR.getText();
		int iNummer = Integer.parseInt(sNummer);
		
		// Alternative Kurzform
		// int iNummer Integer.parseInt(jtfNR.getText());
		
		Auto meinAuto = meineDBV.sucheAutoperNummer(iNummer);
		
		if (meinAuto == null)
		{
			jtfMarke.setForeground(Color.RED);
			jtfModell.setForeground(Color.RED);
			jtfPreis.setForeground(Color.RED);
			jtfMarke.setText("Es wurde kein");
			jtfModell.setText("Auto mit der");
			jtfPreis.setText("Nummer "+iNummer+" gefunden!");
		}
		else
		{
			jtfMarke.setForeground(Color.BLACK);
			jtfModell.setForeground(Color.BLACK);
			jtfPreis.setForeground(Color.BLACK);
			jtfMarke.setText(meinAuto.getMarke());
			jtfModell.setText(meinAuto.getModell());
			jtfPreis.setText(meinAuto.getPreis()+"");
			
			// Aufwendigere Alternative:
			// double dPreis = meinAuto.getPreis();
			// String sPreis = String.valueOf(dPreis);
			// jtfPreis.setText(sPreis);
		}
		
	}

	

}
