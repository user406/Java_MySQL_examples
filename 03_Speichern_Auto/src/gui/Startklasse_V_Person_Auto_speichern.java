package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import fachklasse.Auto;
import fachklasse.Datenbankverbindung;
import fachklasse.Person;

public class Startklasse_V_Person_Auto_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlPerson;
	private JLabel jlAuto;
	private JLabel jlAusgabe;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;
	private JComboBox<String> meineJCBPerson;
	private JComboBox<String> meineJCBAuto;
	private ArrayList<Person> meinePersonenListe;
	private ArrayList<Auto> meineAutoListe;
	
	public static void main(String[] args)
	{
		new Startklasse_V_Person_Auto_speichern().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Person und Auto verkn�pfen");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlPerson = new JLabel();
		jlPerson.setBounds(20, 20, 200, 20);
		jlPerson.setText("Person: ");
		frame.add(jlPerson);
		
		jlAuto = new JLabel();
		jlAuto.setBounds(20, 60, 200, 20);
		jlAuto.setText("Auto: ");
		frame.add(jlAuto);
		
		meineJCBPerson = new JComboBox<>();
		meineJCBPerson.setBounds(120, 20, 200, 20);
		meineJCBPerson.addItem("Bitte eine Person ausw�hlen");
		frame.add(meineJCBPerson);
		
		meineJCBAuto = new JComboBox<>();
		meineJCBAuto.setBounds(120, 60, 200, 20);
		meineJCBAuto.addItem("Bitte ein Auto ausw�hlen");
		frame.add(meineJCBAuto);
		
		//************************************************
		// ComboBox f�llen
		meineDBV = new Datenbankverbindung();
		meinePersonenListe = meineDBV.sucheAllePersonen();
		for (int i = 0; i < meinePersonenListe.size(); i++)
		{
			Person meinePerson = meinePersonenListe.get(i);
			meineJCBPerson.addItem(meinePerson.getiNr()+" "+meinePerson.getsVorname()+" "+meinePerson.getsNachname());
		}

		
		meineAutoListe = meineDBV.sucheAlleAutos();
		for (int i = 0; i < meineAutoListe.size(); i++)
		{
			Auto meinAuto = meineAutoListe.get(i);
			meineJCBAuto.addItem(meinAuto.getNummer()+" "+meinAuto.getMarke()+" "+meinAuto.getModell()+" "+meinAuto.getPreis());
		}
		
		
		//************************************************
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 140, 270, 26);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// ComboBox Person auslesen
		int iZeilePerson = meineJCBPerson.getSelectedIndex();
		Person meinePerson = meinePersonenListe.get(iZeilePerson-1);
		
		// ComboBox KFZ auslesen
		int iZeileAuto = meineJCBAuto.getSelectedIndex();
		Auto meinAuto = meineAutoListe.get(iZeileAuto-1);
		
		// Die beiden Zahlen (KFZ-Nr und Personen-Nr) in die Datenbank speichern
		boolean bHatGeklappt = meineDBV.speichere_V_Person_Auto(meinePerson.getiNr(), meinAuto.getNummer());
		
		if (bHatGeklappt == true)
		{
			jlAusgabe.setForeground(Color.BLACK);
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Speichern fehlgeschlagen");
		}
		
	}

}
