package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Auto_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlMarke;
	private JLabel jlModell;
	private JLabel jlPreis;
	private JLabel jlAusgabe;
	private JTextField tfMarke;
	private JTextField tfModell;
	private JTextField tfPreis;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;
	
	public static void main(String[] args)
	{
		new Startklasse_Auto_speichern().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Auto speichern");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlMarke = new JLabel();
		jlMarke.setBounds(20, 20, 100, 20);
		jlMarke.setText("Marke: ");
		frame.add(jlMarke);
		
		jlModell = new JLabel();
		jlModell.setBounds(20, 60, 100, 20);
		jlModell.setText("Modell: ");
		frame.add(jlModell);
		
		jlPreis = new JLabel();
		jlPreis.setBounds(20, 100, 100, 20);
		jlPreis.setText("Preis: ");
		frame.add(jlPreis);
		
		tfMarke = new JTextField();
		tfMarke.setBounds(120, 20, 150, 20);
		frame.add(tfMarke);
		
		tfModell = new JTextField();
		tfModell.setBounds(120, 60, 150, 20);
		frame.add(tfModell);
		
		tfPreis = new JTextField();
		tfPreis.setBounds(120, 100, 150, 20);
		frame.add(tfPreis);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 140, 250, 26);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sMarke = tfMarke.getText();
		String sModell = tfModell.getText();
		String sPreis = tfPreis.getText();
		double dPreis = Double.parseDouble(sPreis);
		
		meineDBV = new Datenbankverbindung();
		
		boolean bHatgeklappt = meineDBV.speichereAutoDaten(sMarke, sModell, dPreis);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setForeground(Color.BLACK);
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern");
		}
		
	}

}
