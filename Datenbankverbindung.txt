	private Connection meineConnection = null;
	private Statement meinStatement = null;
	
	// Methode, die sich mit der Datenbank verbindet
		private void verbindeMitMySQL()
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				System.out.println("Drivermanager geladen, Treibername ok");
				
				try
				{
					meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_bibliothek?user=root&password=");
					meinStatement = meineConnection.createStatement();
					System.out.println("Connection etabliert");
					
				}
				catch (SQLException eSQL)
				{
					System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
					System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
					System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
				System.out.println("Treibername falsch - VendorError: \n" + e.toString());
			}
		}
		
		// Methode, die die MySQL-Verbindung beendet
		private void beendeMySQLVerbindung()
		{
			try
			{
				meinStatement.close();
				meineConnection.close();
				System.out.println("Verbindung beendet");
			}
			catch (SQLException e)
			{
				System.out.println("Fehler beim Beenden der Verbindung :\n"+e);
			}
		}

		// Methode, die Daten aus der Datenbank ausliest
		private ResultSet leseDaten (String sSQLAnweisung)
		{
			ResultSet meinRS = null;
			try
			{
				meinRS = meinStatement.executeQuery(sSQLAnweisung);
				System.out.println("Leseanfrage ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler bei der Leseanfrage: \n"+eSQL);
				meinRS = null;
			}
			return meinRS;
		}
		
		// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der ver�nderten Zeilen in der 
		// Datenbank zur�ck
		private int schreibeDaten (String sSQLAnweisung)
		{
			int iZeilen = 0;
			try
			{
				iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
				System.out.println("Schreibbefehl ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler beim Schreibbefehl: \n"+eSQL);
				System.out.println("\n "+sSQLAnweisung);
			}
			return iZeilen;
		}
