package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Autor;
import fachklasse.Datenbankverbindung;

public class Startklasse_Autor_Suchen_Per_Name implements ActionListener
{
	private JFrame frame;
	private JLabel jlName;
	private JTextField jtfName;
	private JButton jbSuchen;
	private JTextArea jtaAusgabe;
	private JScrollPane jspAusgabe;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Autor_Suchen_Per_Name().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Autor suchen");
		frame.setBounds(400, 300, 350, 400);
		
		jlName = new JLabel();
		jlName.setBounds(20, 20, 150, 20);
		jlName.setText("Vorname / Nachname: ");
		frame.add(jlName);
		
		jtfName = new JTextField();
		jtfName.setBounds(160, 20, 150, 20);
		frame.add(jtfName);
		
		jbSuchen = new JButton();
		jbSuchen.setText("Suchen");
		jbSuchen.addActionListener(this);
		jbSuchen.setBounds(20, 60, 290, 25);
		frame.add(jbSuchen);
		
		jtaAusgabe = new JTextArea();
		jspAusgabe = new JScrollPane();
		jspAusgabe.setBounds(20, 100, 290, 250);
		jspAusgabe.setViewportView(jtaAusgabe);
		frame.add(jspAusgabe);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sName = jtfName.getText();
		
		meineDBV = new Datenbankverbindung();
		
		ArrayList<Autor> meineAutorListe = new ArrayList<>();
		meineAutorListe = meineDBV.sucheAutorPerName(sName);
		
		if (meineAutorListe.size() == 0)
		{
			jtaAusgabe.setForeground(Color.RED);
			jtaAusgabe.setFont(jtaAusgabe.getFont().deriveFont(Font.BOLD));
			jtaAusgabe.setText("Keinen Autor gefunden.");
		}
		else
		{
			jtaAusgabe.setForeground(Color.BLACK);
			jtaAusgabe.setFont(jtaAusgabe.getFont().deriveFont(Font.BOLD));
			jtaAusgabe.setText("Folgende Autoren gefunden: \n");
			for (int i = 0; i < meineAutorListe.size(); i++)
			{
				Autor meinAutor = meineAutorListe.get(i);
				jtaAusgabe.append("Nr: " + meinAutor.getiNr() + " | " + "Vorname: " + meinAutor.getsVorname() + " | " + "Nachname: " + meinAutor.getsNachname() + "\n");
			}
		}
		
	}

}
