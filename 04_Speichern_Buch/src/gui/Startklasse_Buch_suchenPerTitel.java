package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.color.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Buch;
import fachklasse.Datenbankverbindung;

public class Startklasse_Buch_suchenPerTitel implements ActionListener
{
	// Dinge ank�ndigen
	private JFrame frame;
	private JButton jbSuchen;
	private JLabel jlTitel;
	private JTextField jtfTitel;
	private JTextArea jtaAusgabe;
	private JScrollPane jspAusgabe;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Buch_suchenPerTitel().go();

	}

	private void go()
	{
		// Frame erstellen
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Buch suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 350, 300);
			
		// Button, der die Suche in der ActionPerformed Methode startet
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 230, 26);
		frame.add(jbSuchen);
				
		// Label mit  Inhalt "Marke" erstellen
		jlTitel = new JLabel();
		jlTitel.setBounds(20, 20, 100, 20);
		jlTitel.setText("Titel: ");
		frame.add(jlTitel);
				
		// Textfeld, in welchem die gefundene Marke des Autos erscheint
		jtfTitel = new JTextField();
		jtfTitel.setBounds(100, 20, 150, 20);
		jtfTitel.setEditable(true);
		frame.add(jtfTitel);
		
		jtaAusgabe = new JTextArea();
		jtaAusgabe.setEditable(false);
		jspAusgabe = new JScrollPane();
		jspAusgabe.setBounds(20, 100, 300, 150);
		jspAusgabe.setViewportView(jtaAusgabe);
		frame.add(jspAusgabe);
		
		// Frame erscheinen lassen
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
		
		ArrayList<Buch> meineBuchListe = meineDBV.sucheBuchperTitel(jtfTitel.getText());
		
		if (meineBuchListe.size() == 0)
		{
			jtaAusgabe.setForeground(Color.RED);
			jtaAusgabe.setText("Kein Buch gefunden.");
		}
		else
		{
			jtaAusgabe.setForeground(Color.BLACK);
			jtaAusgabe.setText("Folgende B�cher gefunden: \n");
			for (int i = 0; i < meineBuchListe.size(); i++)
			{
				Buch meinBuch = meineBuchListe.get(i);
				jtaAusgabe.append("Nr: "+ meinBuch.getNummer() +"|" +" "+ "ISBN: "+ meinBuch.getISBN() +"|" +" "+ "Titel: "+ meinBuch.getTitel() +"|" +" "+ "Seitenzahl: "+ meinBuch.getSeitenzahl() + "\n");
			}
		}
		
	}

}
