package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Buch_speichern implements ActionListener
{
	// Dinge ankündigen
	private JFrame frame;
	private JButton jbSpeichern;
	private JLabel jlISBN;
	private JLabel jlTitel;
	private JLabel jlSeitenzahl;
	private JTextField jtfISBN;
	private JTextField jtfTitel;
	private JTextField jtfSeitenzahl;
	private JLabel jlAusgabe;
	private Datenbankverbindung meineDBV;
	

	public static void main(String[] args)
	{
		new Startklasse_Buch_speichern().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Buch speichern");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlISBN = new JLabel();
		jlISBN.setBounds(20, 20, 100, 20);
		jlISBN.setText("ISBN: ");
		frame.add(jlISBN);
		
		jlTitel = new JLabel();
		jlTitel.setBounds(20, 60, 100, 20);
		jlTitel.setText("Titel: ");
		frame.add(jlTitel);
		
		jlSeitenzahl = new JLabel();
		jlSeitenzahl.setBounds(20, 100, 100, 20);
		jlSeitenzahl.setText("Seitenzahl: ");
		frame.add(jlSeitenzahl);
		
		jtfISBN = new JTextField();
		jtfISBN.setBounds(120, 20, 150, 20);
		frame.add(jtfISBN);
		
		jtfTitel = new JTextField();
		jtfTitel.setBounds(120, 60, 150, 20);
		frame.add(jtfTitel);
		
		jtfSeitenzahl = new JTextField();
		jtfSeitenzahl.setBounds(120, 100, 150, 20);
		frame.add(jtfSeitenzahl);
		
		jbSpeichern = new JButton();
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		jbSpeichern.setBounds(20, 140, 250, 26);
		frame.add(jbSpeichern);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 150, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sISBN = jtfISBN.getText();
		String sTitel = jtfTitel.getText();
		String sSeitenzahl = jtfSeitenzahl.getText();
		int iSeitenzahl = Integer.parseInt(sSeitenzahl);
		
		meineDBV = new Datenbankverbindung();
		
		boolean bHatgeklappt = meineDBV.speichereBuch(sISBN, sTitel, iSeitenzahl);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setForeground(Color.GREEN);
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim SPeichern");
		}
		
	}
	
	
	

}
