package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Autor_speichern implements ActionListener
{
	// Dinge ankündigen
	private JFrame frame;
	private JButton jbSpeichern;
	private JLabel jlVorname;
	private JLabel jlNachname;
	private JTextField jtfVorname;
	private JTextField jtfNachname;
	private JLabel jlAusgabe;
	private Datenbankverbindung meineDBV;
	

	public static void main(String[] args)
	{
		new Startklasse_Autor_speichern().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Autor speichern");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 20, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jlNachname = new JLabel();
		jlNachname.setBounds(20, 60, 100, 20);
		jlNachname.setText("Nachname: ");
		frame.add(jlNachname);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(120, 20, 150, 20);
		frame.add(jtfVorname);
		
		jtfNachname = new JTextField();
		jtfNachname.setBounds(120, 60, 150, 20);
		frame.add(jtfNachname);
		
		jbSpeichern = new JButton();
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		jbSpeichern.setBounds(20, 140, 250, 26);
		frame.add(jbSpeichern);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 150, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sVorname = jtfVorname.getText();
		String sNachname = jtfNachname.getText();
		
		meineDBV = new Datenbankverbindung();
		
		boolean bHatgeklappt = meineDBV.speichereAutor(sVorname, sNachname);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setText("Speichern erfolgreich");
			Startklasse_Autor_speichern.main(null);
			frame.dispose();
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim SPeichern");
		}
		
	}
	
	
	

}
