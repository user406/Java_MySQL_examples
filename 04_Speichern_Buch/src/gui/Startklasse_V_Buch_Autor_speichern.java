package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Autor;
import fachklasse.Buch;
import fachklasse.Datenbankverbindung;

public class Startklasse_V_Buch_Autor_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlBuch;
	private JLabel jlAutor;
	private JLabel jlAusgabe;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;
	private JComboBox<String> meineJCBBuch;
	private JComboBox<String> meineJCBAutor;
	private ArrayList<Buch> meineBuchListe;
	private ArrayList<Autor> meineAutorListe;
	
	public static void main(String[] args)
	{
		new Startklasse_V_Buch_Autor_speichern().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setTitle("Buch und Autor verkn�pfen");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setLayout(null);
		
		jlBuch = new JLabel();
		jlBuch.setBounds(20, 20, 200, 20);
		jlBuch.setText("Buch: ");
		frame.add(jlBuch);
		
		jlAutor = new JLabel();
		jlAutor.setBounds(20, 60, 200, 20);
		jlAutor.setText("Autor: ");
		frame.add(jlAutor);
		
		meineJCBBuch = new JComboBox<>();
		meineJCBBuch.setBounds(120, 20, 200, 20);
		meineJCBBuch.addItem("Bitte ein Buch ausw�hlen");
		frame.add(meineJCBBuch);
		
		meineJCBAutor = new JComboBox<>();
		meineJCBAutor.setBounds(120, 60, 200, 20);
		meineJCBAutor.addItem("Bitte einen Autor ausw�hlen");
		frame.add(meineJCBAutor);
		
		//************************************************
		// ComboBox f�llen
		meineDBV = new Datenbankverbindung();
		meineBuchListe = meineDBV.sucheAlleBuecher();
		for (int i = 0; i < meineBuchListe.size(); i++)
		{
			Buch meinBuch = meineBuchListe.get(i);
			meineJCBBuch.addItem(meinBuch.getNummer()+" "+meinBuch.getISBN()+" "+meinBuch.getTitel()+" "+meinBuch.getSeitenzahl());
		}

		
		meineAutorListe = meineDBV.sucheAlleAutoren();
		for (int i = 0; i < meineAutorListe.size(); i++)
		{
			Autor meinAutor = meineAutorListe.get(i);
			meineJCBAutor.addItem(meinAutor.getiNr()+" "+meinAutor.getsVorname()+" "+meinAutor.getsNachname());
		}
		
		
		//************************************************
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 200, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 140, 270, 26);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// ComboBox Person auslesen
		int iZeileBuch = meineJCBBuch.getSelectedIndex();
		Buch meinBuch = meineBuchListe.get(iZeileBuch-1);
		
		// ComboBox KFZ auslesen
		int iZeileAutor = meineJCBAutor.getSelectedIndex();
		Autor meinAutor = meineAutorListe.get(iZeileAutor-1);
		
		// Die beiden Zahlen (KFZ-Nr und Personen-Nr) in die Datenbank speichern
		boolean bHatGeklappt = meineDBV.speichere_V_Buch_Autor(meinBuch.getNummer(), meinAutor.getiNr());
		
		if (bHatGeklappt == true)
		{
			jlAusgabe.setText("Speichern erfolgreich");
			Startklasse_V_Buch_Autor_speichern.main(null);  
			frame.dispose();
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Speichern fehlgeschlagen");
		}
		
	}

}
