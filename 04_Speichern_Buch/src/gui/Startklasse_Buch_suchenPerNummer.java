package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.color.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Buch;
import fachklasse.Datenbankverbindung;

public class Startklasse_Buch_suchenPerNummer implements ActionListener
{
	// Dinge ankündigen
	private JFrame frame;
	private JLabel jlNR;
	private JButton jbSuchen;
	private JLabel jlTitel;
	private JLabel jlISBN;
	private JLabel jlSeitenzahl;
	private JTextField jtfTitel;
	private JTextField jtfISBN;
	private JTextField jtfNR;
	private JTextField jtfSeitenzahl;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Buch_suchenPerNummer().go();

	}

	private void go()
	{
		// Frame erstellen
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Buch suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 350, 300);
			
		// Label mit Inhalt "Nummer" erstellen
		jlNR = new JLabel();
		jlNR.setBounds(20, 20, 100, 20);
		jlNR.setText("Nr: ");
		frame.add(jlNR);
				
		// Textfeld, in welches die zu suchende Nummer eingetragen wird
		jtfNR = new JTextField();
		jtfNR.setBounds(100, 20, 150, 20);
		frame.add(jtfNR);
				
		// Button, der die Suche in der ActionPerformed Methode startet
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 230, 26);
		frame.add(jbSuchen);
				
		// Label mit  Inhalt "Marke" erstellen
		jlTitel = new JLabel();
		jlTitel.setBounds(20, 120, 100, 20);
		jlTitel.setText("Titel: ");
		frame.add(jlTitel);
				
		// Label mit Inhalt "Modell" erstellen
		jlISBN = new JLabel();
		jlISBN.setBounds(20, 160, 100, 20);
		jlISBN.setText("ISBN: ");
		frame.add(jlISBN);
				
		// Textfeld, in welchem die gefundene Marke des Autos erscheint
		jtfTitel = new JTextField();
		jtfTitel.setBounds(100, 120, 150, 20);
		jtfTitel.setEditable(false);
		frame.add(jtfTitel);
				
		// Textfeld, in welchem das gefundene Modell des Autos erscheint
		jtfISBN = new JTextField();
		jtfISBN.setBounds(100, 160, 150, 20);
		jtfISBN.setEditable(false);
		frame.add(jtfISBN);
				
		// Label mit Inhalt "Preis" erstellen
		jlSeitenzahl = new JLabel();
		jlSeitenzahl.setBounds(20, 200, 100, 20);
		jlSeitenzahl.setText("Seitenzahl: ");
		frame.add(jlSeitenzahl);
				
		// Textfeld, in welchem der gefundene Preis des Autos erscheint
		jtfSeitenzahl = new JTextField();
		jtfSeitenzahl.setBounds(100, 200, 150, 20);
		jtfSeitenzahl.setEditable(false);
		frame.add(jtfSeitenzahl);
		
		// Frame erscheinen lassen
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
		
		String sNummer = jtfNR.getText();
		int iNummer = Integer.parseInt(sNummer);
		
		Buch meinBuch = meineDBV.sucheBuchperNummer(iNummer);
		
		if (meinBuch == null)
		{
			jtfTitel.setForeground(Color.RED);
			jtfISBN.setForeground(Color.RED);
			jtfSeitenzahl.setForeground(Color.RED);
			jtfTitel.setText("Es wurde kein");
			jtfISBN.setText("Buch mit der");
			jtfSeitenzahl.setText("Nummer "+iNummer+" gefunden!");
		}
		else
		{
			jtfTitel.setForeground(Color.BLACK);
			jtfISBN.setForeground(Color.BLACK);
			jtfSeitenzahl.setForeground(Color.BLACK);
			jtfTitel.setText(meinBuch.getTitel());
			jtfISBN.setText(meinBuch.getISBN());
			jtfSeitenzahl.setText(meinBuch.getSeitenzahl()+"");
		}
		
	}

}
