package fachklasse;

public class Buch
{
	private int iNummer;
	private String sISBN;
	private String sTitel;
	private int iSeitenzahl;
	
	public int getNummer()
	{
		return iNummer;
	}
	
	public String getISBN()
	{
		return sISBN;
	}
	
	public String getTitel()
	{
		return sTitel;
	}
	
	public int getSeitenzahl()
	{
		return iSeitenzahl;
	}
	
	public void setNummer (int iNr)
	{
		iNummer = iNr;
	}
	
	public void setISBN (String sisbn)
	{
		sISBN = sisbn;
	}
	
	public void setTitel (String sTit)
	{
		sTitel = sTit;
	}
	
	public void setSeitenzahl (int isz)
	{
		iSeitenzahl = isz;
	}
	
}
