package fachklasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.util.ResultSetUtil;

public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;
	
	// Methode, die sich mit der Datenbank verbindet
		private void verbindeMitMySQL()
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				System.out.println("Drivermanager geladen, Treibername ok");
				
				try
				{
					meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_bibliothek?user=root&password=");
					meinStatement = meineConnection.createStatement();
					System.out.println("Connection etabliert");
					
				}
				catch (SQLException eSQL)
				{
					System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
					System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
					System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
				System.out.println("Treibername falsch - VendorError: \n" + e.toString());
			}
		}
		
		// Methode, die die MySQL-Verbindung beendet
		private void beendeMySQLVerbindung()
		{
			try
			{
				meinStatement.close();
				meineConnection.close();
				System.out.println("Verbindung beendet");
			}
			catch (SQLException e)
			{
				System.out.println("Fehler beim Beenden der Verbindung :\n"+e);
			}
		}

		// Methode, die Daten aus der Datenbank ausliest
		private ResultSet leseDaten (String sSQLAnweisung)
		{
			ResultSet meinRS = null;
			try
			{
				meinRS = meinStatement.executeQuery(sSQLAnweisung);
				System.out.println("Leseanfrage ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler bei der Leseanfrage: \n"+eSQL);
				meinRS = null;
			}
			return meinRS;
		}
		
		// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der ver�nderten Zeilen in der 
		// Datenbank zur�ck
		private int schreibeDaten (String sSQLAnweisung)
		{
			int iZeilen = 0;
			try
			{
				iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
				System.out.println("Schreibbefehl ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler beim Schreibbefehl: \n"+eSQL);
				System.out.println("\n "+sSQLAnweisung);
			}
			return iZeilen;
		}

		//************************************************************************************************
		
		// Methode, die ein Buch (mit ISBN, Titel und Seitenzahl) in Datenbank speichert
		public boolean speichereBuch (String sISBN, String sTitel, int iSeitenzahl)
		{
			boolean bHatgeklappt = false;
			String sSQLAnweisung = "INSERT INTO buch (ISBN, titel, seitenzahl) VALUES ('"+sISBN+"', '"+sTitel+"', '"+iSeitenzahl+"');";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatgeklappt = true;
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Fehler bei der Methode speichereBuch: \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatgeklappt;
		}
		//***************************************************************************************************************
		
		// Methode, die ein Buch anhand der Nummer sucht
		public Buch sucheBuchperNummer (int iNummer)
		{
			Buch meinBuch = null;
			
			String sSQLAnweisung = "SELECT titel, ISBN, seitenzahl FROM buch WHERE nr = "+iNummer+";";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				
				if (meinRS.next() == true)
				{
					meinBuch = new Buch();
					meinBuch.setTitel(meinRS.getString("titel"));
					meinBuch.setISBN(meinRS.getString("ISBN"));
					meinBuch.setSeitenzahl(meinRS.getInt("seitenzahl"));
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Buchs per Nummer\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			
			return meinBuch;
		}
		//**********************************************************************************************
		
		// Methode, die ein Buch anhand des Titels sucht
		public ArrayList<Buch> sucheBuchperTitel (String sTitel)
		{
			ArrayList<Buch> meineBuchListe = null;
			Buch meinBuch = null;
			
			String sSQLAnweisung = "SELECT nr, isbn, titel, seitenzahl FROM buch WHERE titel LIKE '%"+sTitel+"%';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
					
				meineBuchListe = new ArrayList<>();
				
				while (meinRS.next() == true)
				{
					meinBuch = new Buch();
					meinBuch.setNummer(meinRS.getInt("nr"));
					meinBuch.setISBN(meinRS.getString("ISBN"));
					meinBuch.setTitel(meinRS.getString("titel"));
					meinBuch.setSeitenzahl(meinRS.getInt("seitenzahl"));
					meineBuchListe.add(meinBuch);
				}
						
			}
					
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Buchs per Titel: \n"+e.toString());
			}
					
			finally
			{
				this.beendeMySQLVerbindung();
			}
					
			return meineBuchListe;
		}
		
		//**********************************************************************************
		
		// Methode, die einen Autor (mit Vor- und Nachname) in Datenbank speichert
		public boolean speichereAutor (String sVorname, String sNachname)
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung = "INSERT INTO autor (vorname, nachname) VALUES ('"+sVorname+"', '"+sNachname+"');";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Speichern des Autors.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		//**************************************************************************
		
		// Methode, die alle Eintr�ge in der Tabelle Buch sucht
		public ArrayList<Buch> sucheAlleBuecher ()
		{
			ArrayList<Buch> meineBuchListe = new ArrayList<>();
			Buch meinBuch = null;
			
			String sSQLAnweisung = "SELECT nr, isbn, titel, seitenzahl FROM buch;";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				while (meinRS.next() == true)
				{
					meinBuch = new Buch();
					meinBuch.setNummer(meinRS.getInt("nr"));
					meinBuch.setISBN(meinRS.getString("isbn"));
					meinBuch.setTitel(meinRS.getString("titel"));
					meinBuch.setSeitenzahl(meinRS.getInt("seitenzahl"));
					meineBuchListe.add(meinBuch);
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen aller B�cher.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meineBuchListe;
		}
		//*************************************************************************************
		
		// Methode, die alle Eintr�ge in der Tabelle Autor sucht
		public ArrayList<Autor> sucheAlleAutoren ()
		{
			ArrayList<Autor> meineAutorListe = new ArrayList<>();
			Autor meinAutor = null;
					
			String sSQLAnweisung = "SELECT nr, vorname, nachname FROM autor;";
					
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				while (meinRS.next() == true)
				{
					meinAutor = new Autor();
					meinAutor.setiNr(meinRS.getInt("nr"));
					meinAutor.setsVorname(meinRS.getString("vorname"));
					meinAutor.setsNachname(meinRS.getString("nachname"));
					meineAutorListe.add(meinAutor);
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen aller Autoren.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meineAutorListe;
		}
		
		//************************************************************************
		
		// Methode, die Buch-Nr und Autor-Nr in die 
		// Verkn�pfungstabelle speichert
		public boolean speichere_V_Buch_Autor (int iBuchNummer, int iAutorNummer)
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung  = "INSERT INTO v_buch_autor (buch_nr, autor_nr) VALUES ("+iBuchNummer+", "+iAutorNummer+");";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Speichern in die Verkn�pfungstabelle.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		
		//******************************************************************************************
		
		// Methode, die alle Autoren anhand eines Buchstaben ihres Vor- oder
		// Nachname in der Datenbank sucht
		public ArrayList<Autor> sucheAutorPerName (String sName)
		{
			ArrayList<Autor> meineAutorListe = new ArrayList<>();
			Autor meinAutor = null;
			
			String sSQLAnweisung = "SELECT nr, vorname, nachname FROM autor WHERE vorname OR nachname LIKE '%"+sName+"%';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				while (meinRS.next() == true)
				{
					meinAutor = new Autor();
					meinAutor.setiNr(meinRS.getInt("nr"));
					meinAutor.setsVorname(meinRS.getString("vorname"));
					meinAutor.setsNachname(meinRS.getString("nachname"));
					meineAutorListe.add(meinAutor);
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen der Autoren per Name.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meineAutorListe;
		}
		
		
}

