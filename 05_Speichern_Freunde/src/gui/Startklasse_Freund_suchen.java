package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Adresse;
import fachklasse.Datenbankverbindung;
import fachklasse.Freund;
import fachklasse.Telefon;

public class Startklasse_Freund_suchen implements ActionListener
{
	// Dinge ank�ndigen
	private JFrame frame;
	private JLabel jlNR;
	private JButton jbSuchen;
	private JLabel jlVorname;
	private JLabel jlNachname;
	private JLabel jlGeburtsdatum;
	private JLabel jlTelefonnummer;
	private JLabel jlStra�e;
	private JLabel jlPLZ;
	private JLabel jlOrt;
	private JTextField jtfVorname;
	private JTextField jtfNachname;
	private JTextField jtfNR;
	private JTextField jtfGeburtsdatum;
	private JTextField jtfTelefonnummer;
	private JTextField jtfStra�e;
	private JTextField jtfPLZ;
	private JTextField jtfOrt;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Freund_suchen().go();

	}

	private void go()
	{
		// Frame erstellen
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Freund suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 360, 450);
					
		// Label mit Inhalt "Nummer" erstellen
		jlNR = new JLabel();
		jlNR.setBounds(20, 20, 100, 20);
		jlNR.setText("Nr: ");
		frame.add(jlNR);
						
		// Textfeld, in welches die zu suchende Nummer eingetragen wird
		jtfNR = new JTextField();
		jtfNR.setBounds(140, 20, 150, 20);
		frame.add(jtfNR);
						
		// Button, der die Suche in der ActionPerformed Methode startet
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 270, 26);
		frame.add(jbSuchen);
						
		// Label mit  Inhalt "Vorname" erstellen
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 120, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
						
		// Label mit Inhalt "Nachname" erstellen
		jlNachname = new JLabel();
		jlNachname.setBounds(20, 160, 100, 20);
		jlNachname.setText("Nachname: ");
		frame.add(jlNachname);
		
		// Label mit Inhalt "Geburtsdatum" erstellen
		jlGeburtsdatum = new JLabel();
		jlGeburtsdatum.setBounds(20, 200, 100, 20);
		jlGeburtsdatum.setText("Geburtsdatum: ");
		frame.add(jlGeburtsdatum);
		
		// Label mit Inhalt "Telefonnummer" erstellen
		jlTelefonnummer = new JLabel();
		jlTelefonnummer.setBounds(20, 240, 100, 20);
		jlTelefonnummer.setText("Telefonnummer: ");
		frame.add(jlTelefonnummer);
		
		// Label mit Inhalt "Stra�e" erstellen
		jlStra�e = new JLabel();
		jlStra�e.setBounds(20, 280, 100, 20);
		jlStra�e.setText("Stra�e: ");
		frame.add(jlStra�e);
		
		// Label mit Inhalt "Postleitzahl" erstellen
		jlPLZ = new JLabel();
		jlPLZ.setBounds(20, 320, 100, 20);
		jlPLZ.setText("Postleitzahl: ");
		frame.add(jlPLZ);
		
		// Label mit Inhalt "Ort" erstellen
		jlOrt = new JLabel();
		jlOrt.setBounds(20, 360, 100, 20);
		jlOrt.setText("Ort: ");
		frame.add(jlOrt);
						
		// Textfeld, in welchem der gefundene Vorname des Freundes erscheint
		jtfVorname = new JTextField();
		jtfVorname.setBounds(140, 120, 150, 20);
		jtfVorname.setEditable(false);
		frame.add(jtfVorname);
						
		// Textfeld, in welchem der gefundene Nachname des Freundes erscheint
		jtfNachname = new JTextField();
		jtfNachname.setBounds(140, 160, 150, 20);
		jtfNachname.setEditable(false);
		frame.add(jtfNachname);
										
		// Textfeld, in welchem das gefundene Geburtsdatum des Freundes erscheint
		jtfGeburtsdatum = new JTextField();
		jtfGeburtsdatum.setBounds(140, 200, 150, 20);
		jtfGeburtsdatum.setEditable(false);
		frame.add(jtfGeburtsdatum);
		
		// Textfeld, in welchem die gefundene Telefonnummer des Freundes erscheint
		jtfTelefonnummer = new JTextField();
		jtfTelefonnummer.setBounds(140, 240, 150, 20);
		jtfTelefonnummer.setEditable(false);
		frame.add(jtfTelefonnummer);
		
		// Textfeld, in welchem die gefundene Stra�e des Freundes erscheint
		jtfStra�e = new JTextField();
		jtfStra�e.setBounds(140, 280, 150, 20);
		jtfStra�e.setEditable(false);
		frame.add(jtfStra�e);
		
		// Textfeld, in welchem die gefundene PLZ des Freudes erscheint
		jtfPLZ = new JTextField();
		jtfPLZ.setBounds(140, 320, 150, 20);
		jtfPLZ.setEditable(false);
		frame.add(jtfPLZ);
		
		// Textfeld, in welchem der gefundene Ort des Freundes erscheint
		jtfOrt = new JTextField();
		jtfOrt.setBounds(140, 360, 150, 20);
		jtfOrt.setEditable(false);
		frame.add(jtfOrt);
		
		// Frame erscheinen lassen
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
		
		String sNummer = jtfNR.getText();
		int iNummer = Integer.parseInt(sNummer);
		
		Freund meinFreund = meineDBV.sucheFreundperNummer(iNummer);
		
		if (meinFreund == null)
		{
			jtfVorname.setForeground(Color.RED);
			jtfNachname.setForeground(Color.RED);
			jtfGeburtsdatum.setForeground(Color.RED);
			jtfTelefonnummer.setForeground(Color.RED);
			jtfStra�e.setForeground(Color.RED);
			jtfPLZ.setForeground(Color.RED);
			jtfOrt.setForeground(Color.RED);
			jtfVorname.setText("Es konnte");
			jtfNachname.setText("kein");
			jtfGeburtsdatum.setText("Eintrag");
			jtfTelefonnummer.setText(" mit der");
			jtfStra�e.setText("Nummer" + iNummer );
			jtfPLZ.setText("gefunden");
			jtfOrt.setText("werden!");
		}
		else
		{
			jtfVorname.setText(meinFreund.getVorname());
			jtfNachname.setText(meinFreund.getNachname());
			jtfGeburtsdatum.setText(meinFreund.getGeburtsdatum()+"");
		}
				
		Telefon meinTelefon = meineDBV.sucheTelefonperNummer(iNummer);
		
		if (meinTelefon == null)
		{
			jtfVorname.setForeground(Color.RED);
			jtfNachname.setForeground(Color.RED);
			jtfGeburtsdatum.setForeground(Color.RED);
			jtfTelefonnummer.setForeground(Color.RED);
			jtfStra�e.setForeground(Color.RED);
			jtfPLZ.setForeground(Color.RED);
			jtfOrt.setForeground(Color.RED);
			jtfVorname.setText("Es konnte");
			jtfNachname.setText("kein");
			jtfGeburtsdatum.setText("Eintrag");
			jtfTelefonnummer.setText(" mit der");
			jtfStra�e.setText("Nummer" + iNummer );
			jtfPLZ.setText("gefunden");
			jtfOrt.setText("werden!");
		}
		else
		{
			jtfTelefonnummer.setText(meinTelefon.getTelefonnummer());
		}
				
		Adresse meineAdresse = meineDBV.sucheAdresseperNummer(iNummer);
		
		if (meineAdresse == null)
		{
			jtfVorname.setForeground(Color.RED);
			jtfNachname.setForeground(Color.RED);
			jtfGeburtsdatum.setForeground(Color.RED);
			jtfTelefonnummer.setForeground(Color.RED);
			jtfStra�e.setForeground(Color.RED);
			jtfPLZ.setForeground(Color.RED);
			jtfOrt.setForeground(Color.RED);
			jtfVorname.setText("Es konnte");
			jtfNachname.setText("kein");
			jtfGeburtsdatum.setText("Eintrag");
			jtfTelefonnummer.setText(" mit der");
			jtfStra�e.setText("Nummer" + iNummer );
			jtfPLZ.setText("gefunden");
			jtfOrt.setText("werden!");
		}
		else
		{
			jtfStra�e.setText(meineAdresse.getStra�e());
			jtfPLZ.setText(meineAdresse.getPLZ());
			jtfOrt.setText(meineAdresse.getOrt());
		}
		
	}

}
