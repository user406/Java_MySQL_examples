package gui;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Freund_speichern implements ActionListener
{
	private JFrame meinFrame;
	private JButton jbWeiter;
	private JLabel jlVorname;
	private JLabel jlNachname;
	private JLabel jlGeburtsdatum;
	private JLabel jlAusgabe;
	private JTextField jtfVorname;
	private JTextField jtfNachname;
	private JTextField jtfGeburtsdatum;
	private Datenbankverbindung meineDBV;
	private JLabel jlTelefonnummer;
	private JTextField jtfTelefonnummer;
	private JLabel jlStra�e;
	private JLabel jlPLZ;
	private JLabel jlOrt;
	private JTextField jtfStra�e;
	private JTextField jtfPLZ;
	private JTextField jtfOrt;
	private JButton jbHilfe;
	private ImageIcon iiHilfe;
	
	
	public static void main(String[] args)
	{
		new Startklasse_Freund_speichern().go();
	}

	private void go()
	{
		// ImageIcon mit Fragezeichenbild erstellen
		iiHilfe = new ImageIcon("img/fragezeichen.gif");
		
		// Fragezeichenbild passend f�r Hilfebutton zuschneiden
		Image ihilfe = iiHilfe.getImage();
		ihilfe = ihilfe.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		iiHilfe = new ImageIcon(ihilfe);
		
		// Frame erstellen
		meinFrame = new JFrame();
		meinFrame.setTitle("Freunde speichern");
		meinFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meinFrame.setBounds(600, 200, 400, 350);
		meinFrame.setLayout(null);
		
		// Label mit Inhalt "Vorname" erstellen
		jlVorname = new JLabel();
		jlVorname.setText("Vorname: ");
		jlVorname.setBounds(20, 20, 200, 20);
		jlVorname.setVisible(true);
		meinFrame.add(jlVorname);
		
		// Label mit Inhalt "Telefonnummer" erstellen
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jlTelefonnummer = new JLabel();
		jlTelefonnummer.setText("Telefonnummer: ");
		jlTelefonnummer.setBounds(20, 20, 200, 20);
		jlTelefonnummer.setVisible(false);
		meinFrame.add(jlTelefonnummer);
		
		// Label mit Inhalt "Stra�e" erstellen
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jlStra�e = new JLabel();
		jlStra�e.setText("Stra�e: ");
		jlStra�e.setBounds(20, 20, 200, 20);
		jlStra�e.setVisible(false);
		meinFrame.add(jlStra�e);
		
		// Label mit Inhalt "Nachname" erstellen
		jlNachname = new JLabel();
		jlNachname.setText("Nachname: ");
		jlNachname.setBounds(20, 60, 200, 20);
		jlNachname.setVisible(true);
		meinFrame.add(jlNachname);
		
		// Label mit Inhalt "Postleitzahl" erstellen
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jlPLZ = new JLabel();
		jlPLZ.setText("Postleitzahl: ");
		jlPLZ.setBounds(20, 60, 200, 20);
		jlPLZ.setVisible(false);
		meinFrame.add(jlPLZ);
		
		// Label mit Inhalt "Geburtsdatum" erstellen
		jlGeburtsdatum = new JLabel();
		jlGeburtsdatum.setText("Geburtsdatum: ");
		jlGeburtsdatum.setBounds(20, 100, 200, 20);
		jlGeburtsdatum.setVisible(true);
		meinFrame.add(jlGeburtsdatum);
		
		// Label mit Inhalt "Ort" erstellen
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jlOrt = new JLabel();
		jlOrt.setText("Ort: ");
		jlOrt.setBounds(20, 100, 200, 20);
		jlOrt.setVisible(false);
		meinFrame.add(jlOrt);
		
		// Textfeld, in welches der Vorname eingetragen wird
		jtfVorname = new JTextField();
		jtfVorname.setBounds(120, 20, 150, 20);
		jtfVorname.setVisible(true);
		meinFrame.add(jtfVorname);
		
		// Textfeld, in welches die Telefonnummer eingetragen wird
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jtfTelefonnummer = new JTextField();
		jtfTelefonnummer.setBounds(120, 20, 150, 20);
		jtfTelefonnummer.setVisible(false);
		meinFrame.add(jtfTelefonnummer);
		
		// Textfeld, in welches die Stra�e eingetragen wird
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jtfStra�e = new JTextField();
		jtfStra�e.setBounds(120, 20, 150, 20);
		jtfStra�e.setVisible(false);
		meinFrame.add(jtfStra�e);
		
		// Textfeld, in welches der Nachname eingetragen wird
		jtfNachname = new JTextField();
		jtfNachname.setBounds(120, 60, 150, 20);
		jtfNachname.setVisible(true);
		meinFrame.add(jtfNachname);
		
		// Textfeld, in welches die Postleitzahl eingetragen wird
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jtfPLZ = new JTextField();
		jtfPLZ.setBounds(120, 60, 150, 20);
		jtfPLZ.setVisible(false);
		meinFrame.add(jtfPLZ);
		
		// Textfeld, in welches das Geburtsdatum eingetragen wird
		jtfGeburtsdatum = new JTextField();
		jtfGeburtsdatum.setBounds(120, 100, 150, 20);
		jtfGeburtsdatum.setVisible(true);
		meinFrame.add(jtfGeburtsdatum);
		
		// Hilfebutton erstellen, welcher bei Bet�tigung eine Nachricht
		// �ffnet, in der steht, wie das Geburtsdatum eingetragen werden soll
		jbHilfe = new JButton();
		jbHilfe.setIcon(iiHilfe);
		jbHilfe.addActionListener(this);
		jbHilfe.setActionCommand("hilfe");
		jbHilfe.setBounds(290, 100, 20, 20);
		jbHilfe.setVisible(true);
		meinFrame.add(jbHilfe);
	
		// Textfeld, in welches der Ort eingetragen wird
		// Wird erst in der ActionPerformed Methode sichtbar gemacht
		jtfOrt = new JTextField();
		jtfOrt.setBounds(120, 100, 150, 20);
		jtfOrt.setVisible(false);
		meinFrame.add(jtfOrt);
		
		// Button, welcher bei Dr�cken neue Eingabefelder erscheinen l�sst
		jbWeiter = new JButton();
		jbWeiter.setText("Weiter");
		jbWeiter.addActionListener(this);
		jbWeiter.setActionCommand("weiter1");
		jbWeiter.setBounds(20, 140, 250, 26);
		meinFrame.add(jbWeiter);
		
		// R�ckmeldung, ob Speichern geklappt hat 
		jlAusgabe = new JLabel();
		jlAusgabe.setText("---");
		jlAusgabe.setBounds(20, 200, 150, 20);
		meinFrame.add(jlAusgabe);
		
		meinFrame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
		
		String sActionCommand = e.getActionCommand();
		
		int iPrS = 0;
		
		if (sActionCommand == "hilfe")
		{
			JOptionPane jopHilfe = new JOptionPane();
			JOptionPane.showMessageDialog(meinFrame, "Geburtsdatumformat: YYYY-MM-DD");
		}
		
		if (sActionCommand == "weiter1")
		{
			jlTelefonnummer.setVisible(true);
			jtfTelefonnummer.setVisible(true);
			
			jbHilfe.setVisible(false);
			jlVorname.setVisible(false);
			jlNachname.setVisible(false);
			jlGeburtsdatum.setVisible(false);
			jtfVorname.setVisible(false);
			jtfNachname.setVisible(false);
			jtfGeburtsdatum.setVisible(false);
			
			jbWeiter.setActionCommand("weiter2");
			
			String sVorname = jtfVorname.getText();
			String snachname = jtfNachname.getText();
			String sGeburtdatum = jtfGeburtsdatum.getText();
			Date dGeburtsdatum = Date.valueOf(sGeburtdatum);
					
			boolean bHatgeklappt = meineDBV.speichereFreund(sVorname, snachname, dGeburtsdatum);
			
			if (bHatgeklappt == true)
			{
				jlAusgabe.setForeground(Color.GREEN);
				jlAusgabe.setText("Speichern erfolgreich");
			}
			else
			{
				jlAusgabe.setForeground(Color.RED);
				jlAusgabe.setText("Fehler beim Speichern");
			}
					
		}
		
		if (sActionCommand == "weiter2")
		{
			jlStra�e.setVisible(true);
			jlPLZ.setVisible(true);
			jlOrt.setVisible(true);
			jtfStra�e.setVisible(true);
			jtfPLZ.setVisible(true);
			jtfOrt.setVisible(true);
			
			jbHilfe.setVisible(false);
			jlTelefonnummer.setVisible(false);
			jtfTelefonnummer.setVisible(false);
			jlVorname.setVisible(false);
			jlNachname.setVisible(false);
			jlGeburtsdatum.setVisible(false);
			jtfVorname.setVisible(false);
			jtfNachname.setVisible(false);
			jtfGeburtsdatum.setVisible(false);
					
			jbWeiter.setActionCommand("weiter3");
			
			String sTelefonnummer = jtfTelefonnummer.getText();
					
			boolean bHatgeklappt = meineDBV.speichereTelefon(sTelefonnummer);
			
			if (bHatgeklappt == true)
			{
				jlAusgabe.setForeground(Color.GREEN);
				jlAusgabe.setText("Speichern erfolgreich");
			}
			else
			{
				jlAusgabe.setForeground(Color.RED);
				jlAusgabe.setText("Fehler beim Speichern");
			}
				
		}
		
		if (sActionCommand == "weiter3")
		{
			jbHilfe.setVisible(false);
			String sStra�e = jtfStra�e.getText();
			String sPLZ = jtfPLZ.getText();
			String sOrt = jtfOrt.getText();
						
			boolean bHatgeklappt = meineDBV.speichereAdresse(sStra�e, sPLZ, sOrt);
			
			if (bHatgeklappt == true)
			{
				jlAusgabe.setForeground(Color.GREEN);
				jlAusgabe.setText("Speichern erfolgreich");
			}
			else
			{
				jlAusgabe.setForeground(Color.RED);
				jlAusgabe.setText("Fehler beim Speichern");
			}
			
			jbWeiter.removeActionListener(this);
		}
			
	}
	
}
