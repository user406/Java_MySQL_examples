package fachklasse;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.swing.JTextField;

import gui.Startklasse_Freund_speichern;

public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;
	
	// Methode, die sich mit der Datenbank verbindet
		private void verbindeMitMySQL()
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				System.out.println("Drivermanager geladen, Treibername ok");
				
				try
				{
					meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_freunde?user=root&password=");
					meinStatement = meineConnection.createStatement();
					System.out.println("Connection etabliert");
					
				}
				catch (SQLException eSQL)
				{
					System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
					System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
					System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
				System.out.println("Treibername falsch - VendorError: \n" + e.toString());
			}
		}
		
		// Methode, die die MySQL-Verbindung beendet
		private void beendeMySQLVerbindung()
		{
			try
			{
				meinStatement.close();
				meineConnection.close();
				System.out.println("Verbindung beendet");
			}
			catch (SQLException e)
			{
				System.out.println("Fehler beim Beenden der Verbindung :\n"+e);
			}
		}

		// Methode, die Daten aus der Datenbank ausliest
		private ResultSet leseDaten (String sSQLAnweisung)
		{
			ResultSet meinRS = null;
			try
			{
				meinRS = meinStatement.executeQuery(sSQLAnweisung);
				System.out.println("Leseanfrage ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler bei der Leseanfrage: \n"+eSQL);
				meinRS = null;
			}
			return meinRS;
		}
		
		// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der ver�nderten Zeilen in der 
		// Datenbank zur�ck
		private int schreibeDaten (String sSQLAnweisung)
		{
			int iZeilen = 0;
			try
			{
				iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
				System.out.println("Schreibbefehl ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler beim Schreibbefehl: \n"+eSQL);
				System.out.println("\n "+sSQLAnweisung);
			}
			return iZeilen;
		}

		//************************************************************************************************
		
		public boolean speichereFreund (String sVorname, String sNachname, Date dGeburtsdatum)
		{
			boolean bHatgeklappt = false;
			
			String sSQLAnweisung =  "INSERT INTO freund (vorname, nachname, geburtsdatum) VALUES ('"+sVorname+"', '"+sNachname+"', '"+dGeburtsdatum+"');";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatgeklappt = true;
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Fehler bei der Methode speichereBuch: \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatgeklappt;
		}
		//***************************************************************************************************
		
		public boolean speichereTelefon (String sTelefonnummer)
		{
			boolean bHatgeklappt = false;
			
			String sSQLAnweisung = "INSERT INTO telefon (telefonnummer, freund_nr) VALUES ('"+sTelefonnummer+"', (SELECT nr FROM freund));";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatgeklappt = true;
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Fehler bei der Methode speichereBuch: \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatgeklappt;
		}
		//*******************************************************************************************************
		
		public boolean speichereAdresse (String sStra�e, String sPLZ, String sOrt)
		{
			boolean bHatgeklappt = false;
			
			String sSQLAnweisung = "INSERT INTO adresse (stra�e, plz, ort, freund_nr) VALUES ('"+sStra�e+"', '"+sPLZ+"', '"+sOrt+"', (SELECT nr FROM freund));";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatgeklappt = true;
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Fehler bei der Methode speichereBuch: \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatgeklappt;
		}
		//**********************************************************************************************
		public Freund sucheFreundperNummer (int iNummer)
		{
			Freund meinFreund = null;
			
			String sSQLAnweisung = "SELECT vorname, nachname, geburtsdatum FROM freund WHERE nr = '"+iNummer+"';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				
				if (meinRS.next() == true)
				{
					meinFreund = new Freund();
					meinFreund.setVorname(meinRS.getString("vorname"));
					meinFreund.setNachname(meinRS.getString("nachname"));
					meinFreund.setGeburtsdatum(meinRS.getDate("geburtsdatum"));
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Freundes per Nummer \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meinFreund;
		}
		//*******************************************************************************************************
		
		public Telefon sucheTelefonperNummer (int iNummer)
		{
			Telefon meinTelefon = null;
			
			String sSQLAnweisung = "SELECT telefonnummer FROM telefon WHERE freund_nr = '"+iNummer+"';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				
				if (meinRS.next() == true)
				{
					meinTelefon = new Telefon();
					meinTelefon.setTelefonnummer(meinRS.getString("telefonnummer"));
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Telefons per Nummer \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meinTelefon;
		}
		//*********************************************************************************************************
		
		public Adresse sucheAdresseperNummer (int iNummer)
		{
			Adresse meineAdresse = null;
			
			String sSQLAnweisung = "SELECT stra�e, plz, ort FROM adresse WHERE freund_nr = '"+iNummer+"';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				
				if (meinRS.next() == true)
				{
					meineAdresse = new Adresse();
					meineAdresse.setStra�e(meinRS.getString("stra�e"));
					meineAdresse.setPLZ(meinRS.getString("plz"));
					meineAdresse.setOrt(meinRS.getString("ort"));
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen der Adresse per Nummer \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meineAdresse;
		}
		//*********************************************************************************************
		
		public int suchehoechstenPrimaerschluessel()
		{
			int iPrS = 0;
			
			String sSQLAnweisung = "SELECT max(nr) FROM freund;";
			
			try
			{
				this.verbindeMitMySQL();
				
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des h�chsten Prim�rschl�ssels.");
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return iPrS;
		}
		
		
}

