package fachklasse;

import java.sql.Date;

public class Freund
{
	private int iNummer;
	private String sVorname;
	private String sNachname;
	private Date dGeburtsdatum;
	private String sStra�e;
	private String sPLZ;
	private String sOrt;
	
	public int getNummer()
	{
		return iNummer;
	}
	
	public String getVorname()
	{
		return sVorname;
	}
	
	public String getNachname()
	{
		return sNachname;
	}
	
	public Date getGeburtsdatum()
	{
		return dGeburtsdatum;
	}
	
	public void setNummer (int iNr)
	{
		iNummer = iNr;
	}
	
	public void setVorname (String sVorN)
	{
		sVorname = sVorN;
	}
	
	public void setNachname (String sNachN)
	{
		sNachname = sNachN;
	}
	
	public void setGeburtsdatum (Date dGeburtsD)
	{
		dGeburtsdatum = dGeburtsD;
	}
	
}
