package fachklasse;

public class Telefon
{
	private int iNummer;
	private  String sTelefonnummer;
	
	public int getNummer()
	{
		return iNummer;
	}
	
	public String getTelefonnummer()
	{
		return sTelefonnummer;
	}
	
	public void setNummer(int iNr)
	{
		iNummer = iNr;
	}
	
	public void setTelefonnummer(String sTelefonNr)
	{
		sTelefonnummer = sTelefonNr;
	}
}
