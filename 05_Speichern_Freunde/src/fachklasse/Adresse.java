package fachklasse;

public class Adresse
{
	private int iNummer;
	private String sStra�e;
	private String sPLZ;
	private String sOrt;
	
	public String getStra�e()
	{
		return sStra�e;
	}
	
	public String getPLZ()
	{
		return sPLZ;
	}
	
	public String getOrt()
	{
		return sOrt;
	}
	
	public void setStra�e (String sStr)
	{
		sStra�e = sStr;
	}
	
	public void setPLZ (String splz)
	{
		sPLZ = splz;
	}
	
	public void setOrt (String soRT)
	{
		sOrt = soRT;
	}
}
