DROP DATABASE IF EXISTS db_schule;
CREATE DATABASE db_schule;
USE db_schule;

CREATE TABLE schule (sname VARCHAR(30), email VARCHAR(30), 
adresse VARCHAR(30), PRIMARY KEY (sname)) ENGINE = INNODB;

CREATE TABLE klasse (kuerzel VARCHAR(10), bezeichnung VARCHAR(30), 
groesse INTEGER, PRIMARY KEY (kuerzel)) ENGINE = INNODB;

CREATE TABLE schueler (nr INTEGER AUTO_INCREMENT, vorname VARCHAR(20),
nachname VARCHAR(30), klasse_kuerzel VARCHAR(10),
FOREIGN KEY (klasse_kuerzel) REFERENCES klasse(kuerzel),
PRIMARY KEY (nr)) ENGINE = INNODB;