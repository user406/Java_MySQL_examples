package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Klasse_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlKuerz, jlBez, jlGr, jlAusgabe;
	private JTextField jtfKuerz, jtfBez, jtfGr;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Klasse_speichern().go();
	}

	private void go()
	{
		frame =  new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Klasse speichern");
		frame.setBounds(400, 300, 400, 450);
		
		jlKuerz = new JLabel();
		jlKuerz.setBounds(20, 20, 100, 20);
		jlKuerz.setText("K�rzel: ");
		frame.add(jlKuerz);
		
		jlBez = new JLabel();
		jlBez.setBounds(20, 60, 100, 20);
		jlBez.setText("Bezeichnung: ");
		frame.add(jlBez);
		
		jlGr = new JLabel();
		jlGr.setBounds(20, 100, 100, 20);
		jlGr.setText("Gr��e: ");
		frame.add(jlGr);
		
		jtfKuerz = new JTextField();
		jtfKuerz.setBounds(130, 20, 150, 20);
		frame.add(jtfKuerz);
		
		jtfBez = new JTextField();
		jtfBez.setBounds(130, 60, 150, 20);
		frame.add(jtfBez);
		
		jtfGr = new JTextField();
		jtfGr.setBounds(130, 100, 150, 20);
		frame.add(jtfGr);
		
		jbSpeichern = new JButton();
		jbSpeichern.addActionListener(this);
		jbSpeichern.setText("Speichern");
		jbSpeichern.setBounds(20, 140, 260, 25);
		frame.add(jbSpeichern);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 180, 500, 20);
		jlAusgabe.setText("");
		frame.add(jlAusgabe);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sKuerzel = jtfKuerz.getText();
		String sBezeichnung = jtfBez.getText();
		int iGroesse = Integer.parseInt(jtfGr.getText());
		
		meineDBV = new Datenbankverbindung();
		boolean bHatGeklappt = meineDBV.speichereKlasse(sKuerzel, sBezeichnung, iGroesse);
		
		if (bHatGeklappt == true)
		{
			jlAusgabe.setForeground(Color.BLACK);
			jlAusgabe.setText("Speichern erfolgreich.");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern.");
		}
	}

}
