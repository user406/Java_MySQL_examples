package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Klasse;

public class Startklasse_Klasse_suchen_Per_Kuerzel implements ActionListener
{
	private JFrame frame;
	private JLabel jlKuerzel, jlAusgabe;
	private JTextField jtfKuerzel, jtfAusgabe;
	private JButton jbSuchen;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Klasse_suchen_Per_Kuerzel().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Klasse Suchen per K�rzel");
		frame.setBounds(400, 300, 300, 350);
		
		jlKuerzel = new JLabel();
		jlKuerzel.setBounds(20, 20, 100, 20);
		jlKuerzel.setText("K�rzel: ");
		frame.add(jlKuerzel);
		
		jtfKuerzel = new JTextField();
		jtfKuerzel.setBounds(100, 20, 150, 20);
		frame.add(jtfKuerzel);
		
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 230, 25);
		frame.add(jbSuchen);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 110, 100, 20);
		jlAusgabe.setText("K�rzel: ");
		frame.add(jlAusgabe);
		
		jtfAusgabe = new JTextField();
		jtfAusgabe.setBounds(100, 110, 150, 20);
		frame.add(jtfAusgabe);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sKuerzel = jtfKuerzel.getText();
		
		meineDBV = new Datenbankverbindung();
		
		Klasse meineKlasse = new Klasse();
		meineKlasse = meineDBV.sucheKlassePerKuerzel(sKuerzel);
		
		if (meineKlasse == null)
		{
			jtfAusgabe.setForeground(Color.RED);
			jtfAusgabe.setFont(jtfAusgabe.getFont().deriveFont(Font.BOLD));
			jtfAusgabe.setText("Kein Eintrag.");
		}
		else
		{
			jtfAusgabe.setForeground(Color.BLACK);
			jtfAusgabe.setFont(jtfAusgabe.getFont().deriveFont(Font.BOLD));
			jtfAusgabe.setText(meineKlasse.getsKuerzel());
		}
	}

}
