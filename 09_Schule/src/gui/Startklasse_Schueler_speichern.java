package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Schueler_speichern implements ActionListener
{
	private JFrame frame;
	private JLabel jlVorname, jlNachname, jlKlasse, jlAusgabe;
	private JTextField jtfVorname, jtfNachname, jtfKlasse;
	private JButton jbSpeichern;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Schueler_speichern().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Sch�ler speichern");
		frame.setBounds(400, 300, 400, 450);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 20, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jlNachname = new JLabel();
		jlNachname.setBounds(20, 60, 100, 20);
		jlNachname.setText("Nachname: ");
		frame.add(jlNachname);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(120, 20, 150, 20);
		frame.add(jtfVorname);
		
		jtfNachname = new JTextField();
		jtfNachname.setBounds(120, 60, 150, 20);
		frame.add(jtfNachname);
		
		jbSpeichern = new JButton();
		jbSpeichern.addActionListener(this);
		jbSpeichern.setText("Speichern");
		jbSpeichern.setBounds(20, 100, 250, 25);
		frame.add(jbSpeichern);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 140, 500, 20);
		jlAusgabe.setText("");
		frame.add(jlAusgabe);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sVorname = jtfVorname.getText();
		String sNachname = jtfNachname.getText();
		
		meineDBV = new Datenbankverbindung();
		boolean bHatGeklappt = meineDBV.speichereSchueler(sVorname, sNachname);
		
		if (bHatGeklappt == true)
		{
			jlAusgabe.setForeground(Color.BLACK);
			jlAusgabe.setText("Speichern erfolgreich.");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern.");
		}
		
	}

}
