package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Schueler;

public class Startklasse_Schueler_Suchen_Per_Vorame implements ActionListener
{
	private JFrame frame;
	private JLabel jlVorname;
	private JTextField jtfVorname;
	private JButton jbSuchen;
	private JTextArea jtaSchueler;
	private JScrollPane jspSchueler;
	private Datenbankverbindung meineDBV;

	public static void main(String[] args)
	{
		new Startklasse_Schueler_Suchen_Per_Vorame().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setTitle("Sch�ler suchen per Vorname");
		frame.setBounds(400, 300, 300, 350);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 20, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(110, 20, 150, 20);
		frame.add(jtfVorname);
		
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 240, 25);
		frame.add(jbSuchen);
		
		jtaSchueler = new JTextArea();
		jspSchueler = new JScrollPane();
		jspSchueler.setBounds(20, 100, 240, 200);
		jspSchueler.setViewportView(jtaSchueler);
		frame.add(jspSchueler);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String sVorname = jtfVorname.getText();
		
		meineDBV = new Datenbankverbindung();
		
		ArrayList<Schueler> meineSchuelerListe = new ArrayList<>();
		meineSchuelerListe = meineDBV.sucheSchuelerPerVorname(sVorname);
		
		if (meineSchuelerListe.size() == 0)
		{
			jtaSchueler.setForeground(Color.RED);
			jtaSchueler.setFont(jtaSchueler.getFont().deriveFont(Font.BOLD));
			jtaSchueler.setText("Keinen Sch�ler gefunden.");
		}
		else
		{
			jtaSchueler.setForeground(Color.BLACK);
			jtaSchueler.setFont(jtaSchueler.getFont().deriveFont(Font.BOLD));
			jtaSchueler.setText("Folgende Schu�ler gefunden:\n");
			for (int i = 0; i < meineSchuelerListe.size(); i++)
			{
				Schueler meinSchueler = new Schueler();
				meinSchueler = meineSchuelerListe.get(i);
				jtaSchueler.append("Nr: " + meinSchueler.getiNr() + " | " + "Vorname: " + meinSchueler.getsVorname() + " | " + "Nachname: " + meinSchueler.getsNachname() + "\n");
			}
		}
	}

}
