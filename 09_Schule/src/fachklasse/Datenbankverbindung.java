package fachklasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;
	
	// Methode, die sich mit der Datenbank verbindet
		private void verbindeMitMySQL()
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				System.out.println("Drivermanager geladen, Treibername ok");
				
				try
				{
					meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_schule?user=root&password=");
					meinStatement = meineConnection.createStatement();
					System.out.println("Connection etabliert");
					
				}
				catch (SQLException eSQL)
				{
					System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
					System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
					System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
				System.out.println("Treibername falsch - VendorError: \n" + e.toString());
			}
		}
		
		// Methode, die die MySQL-Verbindung beendet
		private void beendeMySQLVerbindung()
		{
			try
			{
				meinStatement.close();
				meineConnection.close();
				System.out.println("Verbindung beendet");
			}
			catch (SQLException e)
			{
				System.out.println("Fehler beim Beenden der Verbindung :\n"+e);
			}
		}

		// Methode, die Daten aus der Datenbank ausliest
		private ResultSet leseDaten (String sSQLAnweisung)
		{
			ResultSet meinRS = null;
			try
			{
				meinRS = meinStatement.executeQuery(sSQLAnweisung);
				System.out.println("Leseanfrage ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler bei der Leseanfrage: \n"+eSQL);
				meinRS = null;
			}
			return meinRS;
		}
		
		// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der ver�nderten Zeilen in der 
		// Datenbank zur�ck
		private int schreibeDaten (String sSQLAnweisung)
		{
			int iZeilen = 0;
			try
			{
				iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
				System.out.println("Schreibbefehl ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler beim Schreibbefehl: \n"+eSQL);
				System.out.println("\n "+sSQLAnweisung);
			}
			return iZeilen;
		}
		
		//************************************************************************************************
		
		// Methode, die einen Sch�ler in die Datenbank speichert
		public boolean speichereSchueler (String sVorname, String sNachname)
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung = "INSERT INTO schueler (vorname, nachname) VALUES ('"+sVorname+"', '"+sNachname+"');";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Speichern des Sch�lers.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		//************************************************************************************************
		
		// Methode, die eine Klasse in die Datenbank speichert
		public boolean speichereKlasse (String sKuerzel, String sBezeichnung, int iGroesse)
		{
			boolean bHatGeklappt = false;
			
			String sSQLAnweisung = "INSERT INTO klasse (kuerzel, bezeichnung, groesse) VALUES ('"+sKuerzel+"', '"+sBezeichnung+"', "+iGroesse+");";
			
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatGeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Speichern der Klasse.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return bHatGeklappt;
		}
		//************************************************************************************************
		
		// Methode, die eine Klasse anhand des K�rzels sucht
		public Klasse sucheKlassePerKuerzel (String sKuerzel)
		{
			Klasse meineKlasse = null;
			
			String sSQLAnweisung = "SELECT kuerzel FROM klasse WHERE kuerzel LIKE '%"+sKuerzel+"%';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				while (meinRS.next() == true)
				{
					meineKlasse = new Klasse();
					meineKlasse.setsKuerzel(meinRS.getString("kuerzel"));
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen der Klasse per K�rzel.\n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meineKlasse;
		}
		
		//************************************************************************************************
		
		// Methode, die einen Sch�ler anhand eines Buchstaben des Vornames sucht
		public ArrayList<Schueler> sucheSchuelerPerVorname (String sVorname)
		{
			ArrayList<Schueler> meineSchuelerListe = new ArrayList<>();
			Schueler meinSchueler = null;
			
			String sSQLAnweisung = "SELECT nr, vorname, nachname FROM schueler WHERE vorname LIKE '%"+sVorname+"%';";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				while (meinRS.next() == true)
				{
					meinSchueler = new Schueler();
					meinSchueler.setiNr(meinRS.getInt("nr"));
					meinSchueler.setsVorname(meinRS.getString("vorname"));
					meinSchueler.setsNachname(meinRS.getString("nachname"));
					meineSchuelerListe.add(meinSchueler);
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen des Sch�lers per Vorname.\n");
			}
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meineSchuelerListe;
		}
		
		//************************************************************************************************


}
