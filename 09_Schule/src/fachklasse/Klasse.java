package fachklasse;

public class Klasse
{
	private String sKuerzel;
	private String sBezeichnung;
	private int iGroesse;
	
	public String getsKuerzel()
	{
		return sKuerzel;
	}
	
	public void setsKuerzel(String sKuerzel)
	{
		this.sKuerzel = sKuerzel;
	}
	
	public String getsBezeichnung()
	{
		return sBezeichnung;
	}
	
	public void setsBezeichnung(String sBezeichnung)
	{
		this.sBezeichnung = sBezeichnung;
	}
	
	public int getiGroesse()
	{
		return iGroesse;
	}
	
	public void setiGroesse(int iGroesse)
	{
		this.iGroesse = iGroesse;
	}
	
}
