package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Person;

public class Startklasse_Name_suchenPerVorname implements ActionListener
{
	private JFrame frame;
	private JButton jbSuchen;
	private JLabel jlVorname;
	private JTextField jtfVorname;
	private JTextArea jtaAusgabe;
	private JScrollPane jspAusgabe;
	private Datenbankverbindung meineDBV;
	
	public static void main(String[] args)
	{
		new Startklasse_Name_suchenPerVorname().go();
	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Name suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 400, 300);
		
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 230, 26);
		frame.add(jbSuchen);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 20, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(100, 20, 150, 20);
		jtfVorname.setEditable(true);
		frame.add(jtfVorname);
		
		jtaAusgabe = new JTextArea();
		jtaAusgabe.setEditable(false);
		jspAusgabe = new JScrollPane();
		jspAusgabe.setBounds(20, 100, 300, 150);
		jspAusgabe.setViewportView(jtaAusgabe);
		frame.add(jspAusgabe);
		
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
	
		ArrayList<Person> meinePersonenListe = meineDBV.suchePersonPerVorname(jtfVorname.getText());
		
		if (meinePersonenListe.size() == 0)
		{
			jtaAusgabe.setForeground(Color.RED);
			jtaAusgabe.setText("Keine Person gefunden.");
		}
		else
		{
			jtaAusgabe.setForeground(Color.BLACK);
			jtaAusgabe.setText("Folgende Personen gefunden: \n");
			
			for (int i = 0; i < meinePersonenListe.size(); i++)
			{
				Person meinePerson = meinePersonenListe.get(i);
				jtaAusgabe.append("Nr: "+ meinePerson.getNummer() +"|" +" "+ "Vorname: "+ meinePerson.getVorname() +"|" +" "+ "Nachname: "+ meinePerson.getNachname() +"|" + "\n");
			}
		}
		
	}

}
