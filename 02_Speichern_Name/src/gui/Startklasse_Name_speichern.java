package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;

public class Startklasse_Name_speichern implements ActionListener
{
	private JTextField tfvorname;
	private JTextField tfnachname;
	private JButton jbSpeichern;
	private JLabel jl1;
	private JLabel jl2;
	private JLabel jlAusgabe;
	private Datenbankverbindung meineDBV;


	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		new Startklasse_Name_speichern().go();
	}

	private void go()
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(600, 200, 350, 300);
		frame.setTitle("Name speichern");
		frame.setLayout(null);
		
		tfvorname = new JTextField();
		tfvorname.setBounds(120, 20, 150, 20);
		frame.add(tfvorname);
		
		tfnachname = new JTextField();
		tfnachname.setBounds(120, 60, 150, 20);
		frame.add(tfnachname);
		
		jbSpeichern = new JButton();
		jbSpeichern.setBounds(20, 100, 250, 30);
		jbSpeichern.setText("Speichern");
		jbSpeichern.addActionListener(this);
		frame.add(jbSpeichern);
		
		jl1 = new JLabel();
		jl1.setBounds(20, 20, 100, 20);
		jl1.setText("Vorname: ");
		frame.add(jl1);
		
		jl2 = new JLabel();
		jl2.setBounds(20, 60, 100, 20);
		jl2.setText("Nachname: ");
		frame.add(jl2);
		
		jlAusgabe = new JLabel();
		jlAusgabe.setBounds(20, 170, 300, 20);
		jlAusgabe.setText("---");
		frame.add(jlAusgabe);
		
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		String sVorname = tfvorname.getText();
		String sNachname = tfnachname.getText();
		
		meineDBV = new Datenbankverbindung();
		
		boolean bHatgeklappt = meineDBV.speichereVorUndNachname(sVorname, sNachname);
		
		if (bHatgeklappt == true)
		{
			jlAusgabe.setForeground(Color.GREEN);
			jlAusgabe.setText("Speichern erfolgreich");
		}
		else
		{
			jlAusgabe.setForeground(Color.RED);
			jlAusgabe.setText("Fehler beim Speichern");
		}
		
		
	} 
}
