package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fachklasse.Datenbankverbindung;
import fachklasse.Person;

public class Startklasse_Name_suchenPerNummer implements ActionListener
{
	private JFrame frame;
	private JButton jbSuchen;
	private JLabel jlNR;
	private JLabel jlVorname;
	private JLabel jlNachname;
	private JTextField jtfNR;
	private JTextField jtfVorname;
	private JTextField jtfNachname;
	private Datenbankverbindung meineDBV;
	
	public static void main(String[] args)
	{
		new Startklasse_Name_suchenPerNummer().go();

	}

	private void go()
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Name suchen");
		frame.setLayout(null);
		frame.setBounds(600, 200, 350, 300);
	
		jlNR = new JLabel();
		jlNR.setBounds(20, 20, 100, 20);
		jlNR.setText("Nr: ");
		frame.add(jlNR);
		
		jtfNR = new JTextField();
		jtfNR.setBounds(100, 20, 150, 20);
		frame.add(jtfNR);
		
		jbSuchen = new JButton();
		jbSuchen.addActionListener(this);
		jbSuchen.setText("Suchen");
		jbSuchen.setBounds(20, 60, 230, 26);
		frame.add(jbSuchen);
		
		jlVorname = new JLabel();
		jlVorname.setBounds(20, 120, 100, 20);
		jlVorname.setText("Vorname: ");
		frame.add(jlVorname);
		
		jlNachname = new JLabel();
		jlNachname.setBounds(20, 160, 100, 20);
		jlNachname.setText("Nachname: ");
		frame.add(jlNachname);
		
		jtfVorname = new JTextField();
		jtfVorname.setBounds(100, 120, 150, 20);
		jtfVorname.setEditable(false);
		frame.add(jtfVorname);
		
		jtfNachname = new JTextField();
		jtfNachname.setBounds(100, 160, 150, 20);
		jtfNachname.setEditable(false);
		frame.add(jtfNachname);
		
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		meineDBV = new Datenbankverbindung();
		
		String sNummer = jtfNR.getText();
		int iNummer = Integer.parseInt(sNummer);  
		
		// Alternative Kurzform
		// int iNummer Integer.parseInt(jtfNR.getText());
	
		Person meinePerson = meineDBV.suchePersonPerNummer(iNummer);
		
		if (meinePerson == null)
		{
			jtfVorname.setForeground(Color.RED);
			jtfNachname.setForeground(Color.RED);
			jtfVorname.setText("keine Daten zur");
			jtfNachname.setText("Nummer gefunden!");
		}
		else
		{
			jtfVorname.setForeground(Color.BLACK);
			jtfNachname.setForeground(Color.BLACK);
			jtfVorname.setText(meinePerson.getVorname());
			jtfNachname.setText(meinePerson.getNachname());
		}
		
		
		
	}

}
