package fachklasse;

public class Person
{
	private int iNummer;
	private String sVorname;
	private String sNachname;
	
	public void setNummer (int iNr)
	{
		iNummer = iNr;
	}
	
	public void setVorname (String sVorN)
	{
		sVorname = sVorN;
	}
	
	public void setNachname (String sNachN)
	{
		sNachname = sNachN;
	}
	
	public int getNummer()
	{
		return iNummer;
	}
	
	public String getVorname()
	{
		return sVorname;
	}
	
	public String getNachname()
	{
		return sNachname;
	}
}
