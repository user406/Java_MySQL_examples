package fachklasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.plaf.metal.MetalInternalFrameTitlePane;



public class Datenbankverbindung
{
	private Connection meineConnection = null;
	private Statement meinStatement = null;
	
	// Methode, die sich mit der Datenbank verbindet
		private void verbindeMitMySQL()
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				System.out.println("Drivermanager geladen, Treibername ok");
				
				try
				{
					meineConnection = DriverManager.getConnection("jdbc:mysql://localhost/db_name?user=root&password=");
					meinStatement = meineConnection.createStatement();
					System.out.println("Connection etabliert");
					
				}
				catch (SQLException eSQL)
				{
					System.out.println("Verbindung fehlgeschlagen - SQLException : \n" + eSQL.getMessage());
					System.out.println("Verbindung fehlgeschlagen - SQLState : \n" + eSQL.getSQLState());
					System.out.println("Verbindung fehlgeschlagen - VendorError : \n" + eSQL.getErrorCode());
				}
				
			}
			catch (Exception e)
			{
				System.out.println("Treibername falsch - Driver: \n" + e.getMessage());
				System.out.println("Treibername falsch - VendorError: \n" + e.toString());
			}
		}
		
		// Methode, die die MySQL-Verbindung beendet
		private void beendeMySQLVerbindung()
		{
			try
			{
				meinStatement.close();
				meineConnection.close();
				System.out.println("Verbindung beendet");
			}
			catch (SQLException e)
			{
				System.out.println("Fehler beim Beenden der Verbindung :\n"+e);
			}
		}

		// Methode, die Daten aus der Datenbank ausliest
		private ResultSet leseDaten (String sSQLAnweisung)
		{
			ResultSet meinRS = null;
			try
			{
				meinRS = meinStatement.executeQuery(sSQLAnweisung);
				System.out.println("Leseanfrage ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler bei der Leseanfrage: \n"+eSQL);
				meinRS = null;
			}
			return meinRS;
		}
		
		// Methode, die Daten in eine Datenbank schreibt. Gibt die Anzahl der ver�nderten Zeilen in der 
		// Datenbank zur�ck
		private int schreibeDaten (String sSQLAnweisung)
		{
			int iZeilen = 0;
			try
			{
				iZeilen = meinStatement.executeUpdate(sSQLAnweisung);
				System.out.println("Schreibbefehl ausgef�hrt: "+sSQLAnweisung);
			}
			catch (SQLException eSQL)
			{
				System.out.println("Fehler beim Schreibbefehl: \n"+eSQL);
				System.out.println("\n "+sSQLAnweisung);
			}
			return iZeilen;
		}
		// ************************************************************************
		
		// Methode, die einen Vornamen und einen Nachnamen in die Datenbank speichert
		// Gibt zur�ck, ob es geklappt hat
		public boolean speichereVorUndNachname (String sVorname, String sNachname)
		{
			boolean bHatgeklappt = false;
			String sSQLAnweisung = "INSERT INTO person (vorname, nachname) VALUES ('"+sVorname+"', '"+sNachname+"');";
			try
			{
				this.verbindeMitMySQL();
				int iZeilen = this.schreibeDaten(sSQLAnweisung);
				if (iZeilen == 1)
				{
					bHatgeklappt = true;
				}
			}
			catch (Exception e)
			{
				System.out.println("Fehler beim Schreiben von Vorname und Nachname: \n"+e.toString());
			}
			finally
			{
				this.beendeMySQLVerbindung();	
			}
			
			return bHatgeklappt;

		}
		
		// *********************************************************************************************** 
		
		//Methode, die eine Nummer bekommt, die Daten dazu in der DB sucht und
		// ein Objekt der Klasse Person (mit Vor- und Nachname) zur�ckgibt
		
		public Person suchePersonPerNummer (int iNummer)
		{
			Person meinePerson = null;
			
			String sSQLAnweisung = "SELECT vorname, nachname FROM person WHERE nr = "+iNummer+";";
			
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
				
				if (meinRS.next() == true)
				{
					meinePerson = new Person();
					meinePerson.setVorname(meinRS.getString("vorname"));
					meinePerson.setNachname(meinRS.getString("nachname"));
				}
				
			}
			
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen der Person per Nummer: \n"+e.toString());
			}
			
			finally
			{
				this.beendeMySQLVerbindung();
			}
			
			return meinePerson;
		}
		
		//Methode, die einen Vornamen bekommt, die Daten dazu in der DB sucht und
		// ein Objekt der Klasse Person (mit nummer, Vor- und Nachname) zur�ckgibt
				
		public ArrayList<Person> suchePersonPerVorname (String sVorname)
		{
			ArrayList<Person> meinePersonenListe = null;
			Person meinePerson = null;
					
			String sSQLAnweisung = "SELECT nr, vorname, nachname FROM person WHERE vorname LIKE '%"+sVorname+"%';";
					
			try
			{
				this.verbindeMitMySQL();
				ResultSet meinRS = this.leseDaten(sSQLAnweisung);
					
				meinePersonenListe = new ArrayList<>();
				
				while (meinRS.next() == true)
				{
					meinePerson = new Person();
					meinePerson.setNummer(meinRS.getInt("nr"));
					meinePerson.setVorname(meinRS.getString("vorname"));
					meinePerson.setNachname(meinRS.getString("nachname"));
					meinePersonenListe.add(meinePerson);
				}
						
			}
					
			catch (Exception e)
			{
				System.out.println("Fehler beim Suchen der Person per Vorname: \n"+e.toString());
			}
					
			finally
			{
				this.beendeMySQLVerbindung();
			}
					
			return meinePersonenListe;
		}
		
		
		
		
		
		
		
		
}
